# Методы Api для работы с услугами

## /Works/PriceLimits

Get метод, не принимает параметров. 

Возвращает самую минимальную цену среди всех услуг и самую максимальную. Используется для формирования фильтра по цене.

**Пример ответа**:
```
{
  "MinPrice": 100,
  "MaxPrice": 1000,
  "ErrorMessage": null,
  "ErrorCode": 0
}
```

## /Works/Categories

Get метод, не принимает параметров.

Возвращает список всех категорий услуг.

**Пример ответа**:
```
{
  "Categories": [
    {
      "Id": 15,
      "Name": "Косметология",
      "CategoryType": 0,
      "ParentCategoryId": null,
      "ChildCategories": [
        {
          "Id": 29,
          "Name": "Биоревитализация",
          "CategoryType": 0,
          "ParentCategoryId": 15,
          "ChildCategories": null
        },
        {
          "Id": 33,
          "Name": "Другие процедуры",
          "CategoryType": 0,
          "ParentCategoryId": 15,
          "ChildCategories": null
        },
        {
          "Id": 32,
          "Name": "Коррекция носогубных складок",
          "CategoryType": 0,
          "ParentCategoryId": 15,
          "ChildCategories": null
        },
        {
          "Id": 28,
          "Name": "Мезотерапия",
          "CategoryType": 0,
          "ParentCategoryId": 15,
          "ChildCategories": null
        },
        {
          "Id": 30,
          "Name": "Неинвазивная карбокситерапия",
          "CategoryType": 0,
          "ParentCategoryId": 15,
          "ChildCategories": null
        },
        {
          "Id": 27,
          "Name": "Пилинг",
          "CategoryType": 0,
          "ParentCategoryId": 15,
          "ChildCategories": null
        },
        {
          "Id": 31,
          "Name": "Увеличение губ",
          "CategoryType": 0,
          "ParentCategoryId": 15,
          "ChildCategories": null
        },
        {
          "Id": 26,
          "Name": "Чистка лица",
          "CategoryType": 0,
          "ParentCategoryId": 15,
          "ChildCategories": null
        }
      ]
    },
    {
      "Id": 14,
      "Name": "Макияж",
      "CategoryType": 0,
      "ParentCategoryId": null,
      "ChildCategories": [
        {
          "Id": 24,
          "Name": "Вечерний макияж",
          "CategoryType": 0,
          "ParentCategoryId": 14,
          "ChildCategories": null
        },
        {
          "Id": 22,
          "Name": "Дневной макияж",
          "CategoryType": 0,
          "ParentCategoryId": 14,
          "ChildCategories": null
        },
        {
          "Id": 23,
          "Name": "Свадебный макияж",
          "CategoryType": 0,
          "ParentCategoryId": 14,
          "ChildCategories": null
        },
        {
          "Id": 25,
          "Name": "Фото-макияж (макияж для фотосессий)",
          "CategoryType": 0,
          "ParentCategoryId": 14,
          "ChildCategories": null
        }
      ]
    },
    {
      "Id": 17,
      "Name": "Массаж",
      "CategoryType": 0,
      "ParentCategoryId": null,
      "ChildCategories": [
        {
          "Id": 39,
          "Name": "Антицеллюлитный массаж",
          "CategoryType": 0,
          "ParentCategoryId": 17,
          "ChildCategories": null
        },
        {
          "Id": 41,
          "Name": "Вакуумный массаж",
          "CategoryType": 0,
          "ParentCategoryId": 17,
          "ChildCategories": null
        },
        {
          "Id": 37,
          "Name": "Классический массаж",
          "CategoryType": 0,
          "ParentCategoryId": 17,
          "ChildCategories": null
        },
        {
          "Id": 38,
          "Name": "Лечебный массаж",
          "CategoryType": 0,
          "ParentCategoryId": 17,
          "ChildCategories": null
        },
        {
          "Id": 40,
          "Name": "Медовый массаж",
          "CategoryType": 0,
          "ParentCategoryId": 17,
          "ChildCategories": null
        }
      ]
    },
    {
      "Id": 7,
      "Name": "Ногтевой сервис",
      "CategoryType": 0,
      "ParentCategoryId": null,
      "ChildCategories": [
        {
          "Id": 9,
          "Name": "Аппаратный маникюр",
          "CategoryType": 0,
          "ParentCategoryId": 7,
          "ChildCategories": null
        },
        {
          "Id": 12,
          "Name": "Аппаратный педикюр",
          "CategoryType": 0,
          "ParentCategoryId": 7,
          "ChildCategories": null
        },
        {
          "Id": 8,
          "Name": "Классический маникюр",
          "CategoryType": 0,
          "ParentCategoryId": 7,
          "ChildCategories": null
        },
        {
          "Id": 11,
          "Name": "Классический педикюр",
          "CategoryType": 0,
          "ParentCategoryId": 7,
          "ChildCategories": null
        },
        {
          "Id": 10,
          "Name": "Комбинированный маникюр",
          "CategoryType": 0,
          "ParentCategoryId": 7,
          "ChildCategories": null
        },
        {
          "Id": 13,
          "Name": "Комбинированный педикюр",
          "CategoryType": 0,
          "ParentCategoryId": 7,
          "ChildCategories": null
        }
      ]
    },
    {
      "Id": 1,
      "Name": "Парикмахерские услуги",
      "CategoryType": 0,
      "ParentCategoryId": null,
      "ChildCategories": [
        {
          "Id": 21,
          "Name": "Выпрямление",
          "CategoryType": 0,
          "ParentCategoryId": 1,
          "ChildCategories": null
        },
        {
          "Id": 4,
          "Name": "Детские стрижки",
          "CategoryType": 0,
          "ParentCategoryId": 1,
          "ChildCategories": null
        },
        {
          "Id": 3,
          "Name": "Женские стрижки",
          "CategoryType": 0,
          "ParentCategoryId": 1,
          "ChildCategories": null
        },
        {
          "Id": 20,
          "Name": "Ламинирование",
          "CategoryType": 0,
          "ParentCategoryId": 1,
          "ChildCategories": null
        },
        {
          "Id": 2,
          "Name": "Мужские стрижки",
          "CategoryType": 0,
          "ParentCategoryId": 1,
          "ChildCategories": null
        },
        {
          "Id": 5,
          "Name": "Окрашивание",
          "CategoryType": 0,
          "ParentCategoryId": 1,
          "ChildCategories": null
        },
        {
          "Id": 6,
          "Name": "Прически",
          "CategoryType": 0,
          "ParentCategoryId": 1,
          "ChildCategories": null
        },
        {
          "Id": 19,
          "Name": "Химическая завивка",
          "CategoryType": 0,
          "ParentCategoryId": 1,
          "ChildCategories": null
        }
      ]
    },
    {
      "Id": 18,
      "Name": "Ресницы",
      "CategoryType": 0,
      "ParentCategoryId": null,
      "ChildCategories": [
        {
          "Id": 43,
          "Name": "Ламинирование ресниц",
          "CategoryType": 0,
          "ParentCategoryId": 18,
          "ChildCategories": null
        },
        {
          "Id": 44,
          "Name": "Наращивание ресниц",
          "CategoryType": 0,
          "ParentCategoryId": 18,
          "ChildCategories": null
        },
        {
          "Id": 42,
          "Name": "Окрашивание ресниц",
          "CategoryType": 0,
          "ParentCategoryId": 18,
          "ChildCategories": null
        }
      ]
    },
    {
      "Id": 16,
      "Name": "Эпиляция",
      "CategoryType": 0,
      "ParentCategoryId": null,
      "ChildCategories": [
        {
          "Id": 34,
          "Name": "Депиляция",
          "CategoryType": 0,
          "ParentCategoryId": 16,
          "ChildCategories": null
        },
        {
          "Id": 36,
          "Name": "Лазерная эпиляция",
          "CategoryType": 0,
          "ParentCategoryId": 16,
          "ChildCategories": null
        },
        {
          "Id": 35,
          "Name": "Шугаринг",
          "CategoryType": 0,
          "ParentCategoryId": 16,
          "ChildCategories": null
        }
      ]
    }
  ],
  "ErrorMessage": null,
  "ErrorCode": 0
}
```