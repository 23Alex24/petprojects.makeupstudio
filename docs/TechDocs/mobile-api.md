# Мобильное Api

На момент написания никакой проверки аутентификации у мобильного апи не было. На это не было времени + нет методов, которые бы выдавали скрытую информацию.

Все запросы и ответы в формате JSON. У каждого метода есть поля ErrorMessage - текст ошибки и ErrorCode - код ошибки. Если код ошибки отличается от 0, значит есть ошибка.
На все ответы код 200, ошибка в самом JSON. Сделано так, а не через HTTP коды, потому что рано или поздно все равно приходится вводить доп. коды ошибок в ответах и в итоге получается, что 
клиентам приходится смотреть и на HTTP код и на код в ответе (p.s. на текущий момент делаю по другому, возвращаю http коды). В данном подходе HTTP код отличный от 200 означает, что произошла какая-то серверная ошибка.

* [Companies](https://gitlab.com/23Alex24/petprojects.makeupstudio/blob/master/docs/TechDocs/companies-api.md)
* [Works](https://gitlab.com/23Alex24/petprojects.makeupstudio/blob/master/docs/TechDocs/works-api.md)
* [Orders](https://gitlab.com/23Alex24/petprojects.makeupstudio/blob/master/docs/TechDocs/orders-api.md)