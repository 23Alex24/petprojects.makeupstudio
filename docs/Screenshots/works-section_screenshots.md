# Раздел услуг

#### **Отображение когда нет добавленных услуг**.

![7](https://gitlab.com/23Alex24/petprojects.makeupstudio/raw/master/images/7.png)

#### **Создание услуги**. При выборе основной услуги загружается выпадающий список подкатегорий. Список подкатегорий формируется исходя из выбранной категории услуг.

![8](https://gitlab.com/23Alex24/petprojects.makeupstudio/raw/master/images/8.png)

#### **Шаблон для названий услуг**. После выбора категории и подкатегории можно ввести название услуги или выбрать из предоставляемого списка. 
Такие списки есть НЕ для всех подкатегорий и в этом случае кнопка выбора становится неактивной.


![9](https://gitlab.com/23Alex24/petprojects.makeupstudio/raw/master/images/9.png)

#### **Ошибки валидации**.

![10](https://gitlab.com/23Alex24/petprojects.makeupstudio/raw/master/images/10.png)

#### **Табличка услуг и вывод после добавления**.

![11](https://gitlab.com/23Alex24/petprojects.makeupstudio/raw/master/images/11.png)

#### **Изменение услуги**. Выбор категорий недоступен. 

![12](https://gitlab.com/23Alex24/petprojects.makeupstudio/raw/master/images/12.png)