# Раздел заказов

#### **Создание заказа**. У мастеров есть возможность создавать вручную заказы, чтобы хранить и обрабатывать заказы, которые были сделаны НЕ с мобильного приложения.

![20](https://gitlab.com/23Alex24/petprojects.makeupstudio/raw/master/images/20.png)

#### **Ошибки валидации и выбор даты и времени**.

![21](https://gitlab.com/23Alex24/petprojects.makeupstudio/raw/master/images/21.png)

![22](https://gitlab.com/23Alex24/petprojects.makeupstudio/raw/master/images/22.png)

#### **Таблица заказов**.

![23](https://gitlab.com/23Alex24/petprojects.makeupstudio/raw/master/images/23.png)

#### **Редактирование заказов**. Номер заказа, комментарий клиента, дату создания заказа редактировать нельзя. При необходимости мастер может добавить свой комментарий.

![24](https://gitlab.com/23Alex24/petprojects.makeupstudio/raw/master/images/24.png)

