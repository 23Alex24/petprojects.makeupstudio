# Скриншоты страницы входа (админка)

**Форма входа адаптивная**

![1](https://gitlab.com/23Alex24/petprojects.makeupstudio/raw/master/images/1.png)

**Вывод ошибок**

![2](https://gitlab.com/23Alex24/petprojects.makeupstudio/raw/master/images/2.png)

**Вывод серверных ошибок**

![3](https://gitlab.com/23Alex24/petprojects.makeupstudio/raw/master/images/3.png)

**Страница регистрации**

![4](https://gitlab.com/23Alex24/petprojects.makeupstudio/raw/master/images/4.png)

**После регистрации**

![5](https://gitlab.com/23Alex24/petprojects.makeupstudio/raw/master/images/5.png)