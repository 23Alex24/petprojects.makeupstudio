# Раздел фотографий

#### Вид страницы без фото

![13](https://gitlab.com/23Alex24/petprojects.makeupstudio/raw/master/images/13.png)

#### Изменение аватарки

![14](https://gitlab.com/23Alex24/petprojects.makeupstudio/raw/master/images/14.png)

#### После добавления аватарки

![15](https://gitlab.com/23Alex24/petprojects.makeupstudio/raw/master/images/15.png)

#### Добавление фотографий

![16](https://gitlab.com/23Alex24/petprojects.makeupstudio/raw/master/images/16.png)

#### Валидация

![17](https://gitlab.com/23Alex24/petprojects.makeupstudio/raw/master/images/17.png)

#### Страница фотографий с загруженными фотографиями на большом экране

![18](https://gitlab.com/23Alex24/petprojects.makeupstudio/raw/master/images/18.png)

#### На маленьких экранах

![19](https://gitlab.com/23Alex24/petprojects.makeupstudio/raw/master/images/19.png)


