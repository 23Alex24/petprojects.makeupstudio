﻿using System;
using System.Threading.Tasks;
using MakeupStudio.Core.Entities;

namespace MakeupStudio.Core.Services
{
    /// <summary>
    /// Сервис для работы с фирмами
    /// </summary>
    public interface ICompanyService
    {
        /// <summary>
        /// Ищет фирмы по определенным критериям
        /// </summary>
        /// <param name="categoriesId">Айди категорий</param>
        /// <param name="minPrice">Минимальная цена услуги</param>
        /// <param name="maxPrice">Максимальная цена услуги</param>
        Task<CompanyPreviewDto[]> SearchCompanies(int[] categoriesId, decimal? minPrice, decimal? maxPrice);

        /// <summary>
        /// Возвращает активные компании, у которых есть услуги
        /// </summary>
        Task<Company[]> GetActiveCompanies();

        /// <summary>
        /// Возвращает статус фирмы
        /// </summary>
        Task<CompanyState?> GetCompanyState(Guid companyId);

        /// <summary>
        /// Получает фирму по ее айди
        /// </summary>
        Task<Company> GetCompany(Guid companyId);

        /// <summary>
        /// Проверяет существует ли фирма
        /// </summary>
        Task<bool> CompanyIsExists(Guid companyId);
    }
}
