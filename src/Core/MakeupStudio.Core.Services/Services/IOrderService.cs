﻿using System;
using System.Threading.Tasks;
using MakeupStudio.Core.Entities;

namespace MakeupStudio.Core.Services
{
    /// <summary>
    /// Сервис для работы с заказами
    /// </summary>
    public interface IOrderService
    {
        /// <summary>
        /// Возвращает новую заявку
        /// </summary>
        Task<Order> GetNotAcceptedOrder(Guid companyId, long orderId);

        /// <summary>
        /// Возвращает новые заявки
        /// </summary>
        /// <param name="companyId">Айди компании</param>
        Task<Order[]> GetNotAcceptedOrders(Guid companyId);

        /// <summary>
        /// Возвращает заказ
        /// </summary>
        Task<Order> GetOrder(Guid companyId, long orderId);

        /// <summary>
        /// Проверяет существует ли уже заявка от пользователя
        /// </summary>
        /// <param name="companyId">Id фирмы</param>
        /// <param name="clientPhone">Телефон клиента</param>
        Task<bool> BidIsExists(Guid companyId, string clientPhone);

        /// <summary>
        /// Можно ли изменить время записи заказа (нет ли уже назначенных заказов на это или близкое время)
        /// </summary>
        /// <param name="companyId">Айди фирмы</param>
        /// <param name="orderId">Айди заказа/заявки</param>
        /// <param name="newAppointedTime">Новое время записи</param>
        Task<bool> CanChangeAppointedTime(Guid companyId, long orderId, DateTime newAppointedTime);

        /// <summary>
        /// Возвращает предстоящие заказы 
        /// </summary>
        Task<Order[]> GetAcceptedOrders(Guid companyId);
    }
}
