﻿using System;
using System.Threading.Tasks;
using MakeupStudio.Core.Entities;

namespace MakeupStudio.Core.Services
{
    /// <summary>
    /// Сервис для работы с аккаунтами
    /// </summary>
    public interface IAccountService
    {
        /// <summary>
        /// Возвращает аккаунт по логину и паролю или null, если он не найден
        /// </summary>
        /// <param name="login">логин</param>
        /// <param name="passwordHash">хеш код пароля пользователя</param>
        Task<Account> GetAccountAsync(string login, string passwordHash);

        /// <summary>
        /// Проверяет есть ли пользователь с таким логином
        /// </summary>
        Task<bool> UserIsExistsAsync(string login);

        /// <summary>
        /// Проверяет используется ли номер телефона
        /// </summary>
        Task<bool> PhoneIsUsed(string phone);

        /// <summary>
        /// Проверяет активна ли фирма и пользователь
        /// </summary>
        Task<bool> UserAndCompanyIsActive(Guid companyId, int accountId);
    }
}
