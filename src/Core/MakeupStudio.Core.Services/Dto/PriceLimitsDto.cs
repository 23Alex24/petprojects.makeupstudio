﻿namespace MakeupStudio.Core.Services
{
    /// <summary>
    /// Информация о минимально и максимально возможных ценах на услуги
    /// </summary>
    public class PriceLimitsDto
    {
        /// <summary>
        /// Минимально возможная цена
        /// </summary>
        public decimal MinPrice { get; set; }

        /// <summary>
        /// Максимально возможная цена
        /// </summary>
        public decimal MaxPrice { get; set; }
    }
}
