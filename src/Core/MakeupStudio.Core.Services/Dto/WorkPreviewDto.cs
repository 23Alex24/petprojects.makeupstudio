﻿namespace MakeupStudio.Core.Services
{
    /// <summary>
    /// Дтошка для краткой информации об услуге
    /// </summary>
    public class WorkPreviewDto
    {
        /// <summary>
        /// Id услуги
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название услуги
        /// </summary>
        public string WorkName { get; set; }

        /// <summary>
        /// Название категории
        /// </summary>
        public string ChildCategoryName { get; set; }

        /// <summary>
        /// Стоимость услуги
        /// </summary>
        public decimal Price { get; set; }
    }
}
