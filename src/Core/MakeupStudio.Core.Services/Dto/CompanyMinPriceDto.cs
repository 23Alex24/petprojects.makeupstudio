﻿using System;

namespace MakeupStudio.Core.Services
{
    /// <summary>
    /// Дтошка для передачи информации о минимальной цене на услугу конкретной фирмы
    /// </summary>
    public class CompanyMinPriceDto
    {
        /// <summary>
        /// Айди фирмы
        /// </summary>
        public Guid CompanyId { get; set; }

        /// <summary>
        /// Минимальная цена на услугу (при наличии хотя бы одной услуги)
        /// </summary>
        public decimal? MinPrice { get; set; }
    }
}
