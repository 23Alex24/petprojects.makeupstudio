﻿using MakeupStudio.Core.Entities;

namespace MakeupStudio.Core.Services
{
    /// <summary>
    /// Информация о категории
    /// </summary>
    public class CategoryInfoDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public CategoryType CategoryType { get; set; }

        public int? ParentCategoryId { get; set; }

        public CategoryInfoDto[] ChildCategories { get; set; }
    }
}
