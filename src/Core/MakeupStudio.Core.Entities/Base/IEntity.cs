﻿namespace MakeupStudio.Core.Entities
{
    /// <summary>
    /// Сущность БД
    /// </summary>
    public interface IEntity
    {

    }

    /// <summary>
    /// Сущность БД
    /// </summary>
    /// <typeparam name="TKey">Тип первичного ключа сущности</typeparam>
    public interface IEntity<TKey> : IEntity
    {
        /// <summary>
        /// Уникальный идентификатор сущности
        /// </summary>
        TKey Id { get; set; }
    }
}