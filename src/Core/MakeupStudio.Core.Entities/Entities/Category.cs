﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace MakeupStudio.Core.Entities
{
    /// <summary>
    /// Категория
    /// </summary>
    [Table("Categories")]
    public class Category : IEntity<int>
    {
        public const int NAME_MAX_LENGTH = 100;



        [Key]
        [Column("Id")]
        public int Id { get; set; }

        /// <summary>
        /// Название категории
        /// </summary>
        [Required]
        [MaxLength(NAME_MAX_LENGTH)]
        [Column("Name")]
        public string Name { get; set; }

        /// <summary>
        /// Тип категории - к чему конкретно эта категория относится
        /// </summary>
        [Column("CategoryType")]
        public CategoryType CategoryType { get; set; }

        /// <summary>
        /// Родительская категория
        /// </summary>
        [ForeignKey(nameof(ParentCategory))]
        [Column("ParentCategoryId")]        
        public int? ParentCategoryId { get; set; }



        #region Навигационные свойства 

        /// <summary>
        /// Навигационное свойство. Родительская категория 
        /// </summary>
        [JsonIgnore]
        public virtual Category ParentCategory { get; set; }

        /// <summary>
        /// Навигационное свойство. Дочерние категории
        /// </summary>
        [JsonIgnore]
        public virtual ICollection<Category> ChildCategories { get; set; }

        /// <summary>
        /// Навигационное свойство. Услуги категории (только для категорий услуг)
        /// </summary>
        [JsonIgnore]
        public virtual ICollection<Work> Works { get; set; }

        /// <summary>
        /// Навигационное свойство. Системные названия услуг
        /// </summary>
        [JsonIgnore]
        public virtual ICollection<SystemWorkName> SystemWorkNames { get; set; }

        #endregion
    }
}