﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace MakeupStudio.Core.Entities
{
    // Пришлось назвать Work, а не Service иначе дикая путаница будет в проекте
    // т.к. сервисы в нашем проекте - классы для работы с сущностями

    /// <summary>
    /// Услуга (например, мужская стрижка). 
    /// </summary>
    [Table("Works")]
    public class Work : IEntity<int>
    {
        public const int NAME_MAX_LENGTH = 100;
        public const decimal MIN_PRICE = 1;
        public const decimal MAX_PRICE = 100000;

        [Key]
        [Column("Id")]
        public int Id { get; set; }

        /// <summary>
        /// Id фирмы чья это услуга
        /// </summary>
        [ForeignKey(nameof(Company))]
        [Column("CompanyId")]
        public Guid CompanyId { get; set; }

        /// <summary>
        /// Id категории, в которой находится услуга
        /// </summary>
        [ForeignKey(nameof(Category))]
        [Column("CategoryId")]
        public int CategoryId { get; set; }

        /// <summary>
        /// Название услуги
        /// </summary>
        [Required]
        [MaxLength(NAME_MAX_LENGTH)]
        [Column("Name")]
        public string Name { get; set; }

        /// <summary>
        /// Стоимость услуги
        /// </summary>
        [Column("Price")]
        public decimal Price { get; set; }



        #region Навигационные свойства

        /// <summary>
        /// Навигационное свойство. Компания, чья это услуга
        /// </summary>
        [JsonIgnore]
        public virtual Company Company { get; set; }

        /// <summary>
        /// Навигационное свойство. Категория, в которой находится услуга
        /// </summary>
        [JsonIgnore]
        public virtual Category Category { get; set; }

        #endregion
    }
}