﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace MakeupStudio.Core.Entities
{
    //Данная сущность нужна только для хранения стандартных названий услуг

    /// <summary>
    /// Системные названия услуг (для хранения дефолтных названий услуг)
    /// </summary>
    [Table("SystemWorkNames")]
    public class SystemWorkName : IEntity<int>
    {
        public const int NAME_MAX_LENGTH = 100;

        [Key]
        [Column("Id")]
        public int Id { get; set; }
        
        /// <summary>
        /// Id категории, в которой находится услуга
        /// </summary>
        [ForeignKey(nameof(Category))]
        [Column("CategoryId")]
        public int CategoryId { get; set; }

        /// <summary>
        /// Название услуги
        /// </summary>
        [Required]
        [MaxLength(NAME_MAX_LENGTH)]
        [Column("Name")]
        public string Name { get; set; }



        #region Навигационные свойства

        /// <summary>
        /// Навигационное свойство. Категория, в которой находится услуга
        /// </summary>
        [JsonIgnore]
        public virtual Category Category { get; set; }

        #endregion
    }
}
