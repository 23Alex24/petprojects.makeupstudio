﻿using System;
using System.Threading.Tasks;
using MakeupStudio.Common.Utils.FileSystem;
using MakeupStudio.Common.Utils.Logger;
using MakeupStudio.Common.Utils.Photos;
using MakeupStudio.Core.Entities;
using MakeupStudio.Core.Services;

namespace MakeupStudio.Business.Commands.Photos
{
    /// <summary>
    /// Команда удаления фотографии
    /// </summary>
    internal class DeletePhotoCommand : BaseCommand<DeletePhotoCommandArgs, EmptyCommandResult>
    {
        #region Конструктор, поля

        private readonly IPhotoService _photoService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPhotosUtility _photosUtility;
        private readonly IFileSystemUtility _fileSystemUtility;
        private Photo _photo;

        public DeletePhotoCommand(
            ILoggerUtility loggerUtility,
            IPhotoService photoService,
            IUnitOfWork unitOfWork,
            IPhotosUtility photosUtility,
            IFileSystemUtility fileSystemUtility) : base(loggerUtility)
        {
            _photoService = photoService;
            _unitOfWork = unitOfWork;
            _photosUtility = photosUtility;
            _fileSystemUtility = fileSystemUtility;
        }

        #endregion


        protected override async Task<EmptyCommandResult> InternalExecute(DeletePhotoCommandArgs arguments)
        {
            var result = new EmptyCommandResult() { IsSuccess = true };
            var repository = _unitOfWork.GetRepository<Photo>();
            var absolutePath = _fileSystemUtility.GetAbsoluteFilePath(_photo.RelativePath);
            repository.Remove(_photo);
            await _unitOfWork.SaveChangesAsync();
            RemovePhotoFile(absolutePath);
            return result;
        }

        protected async override Task<EmptyCommandResult> Validate(DeletePhotoCommandArgs arguments)
        {
            var result = new EmptyCommandResult() {IsSuccess = true};
            _photo = await _photoService.GetPhoto(arguments.CurrentUser.CompanyId, arguments.PhotoId);

            if (_photo == null)
                return result.AddError(Errors.ErrorConstants.Photos.NOT_FOUND, 100);

            return result;
        }


        private void RemovePhotoFile(string absolutePath)
        {
            try
            {
                _fileSystemUtility.DeleteFile(absolutePath);
            }
            catch (Exception ex)
            {
                LoggerUtility.Warn($"Не удалось удалить файл фотографии {absolutePath}", ex);
            }
        }
    }
}
