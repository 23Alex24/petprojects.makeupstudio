﻿using System;
using System.Threading.Tasks;
using MakeupStudio.Common.Utils.FileSystem;
using MakeupStudio.Common.Utils.Logger;
using MakeupStudio.Common.Utils.Photos;
using MakeupStudio.Core.Entities;
using MakeupStudio.Core.Services;

namespace MakeupStudio.Business.Commands.Photos
{
    /// <summary>
    /// Команда изменения аватарки
    /// </summary>
    internal class ChangeAvatarCommand : BaseCommand<ChangeAvatarCommandArgs, EmptyCommandResult>
    {
        private readonly IFileSystemUtility _fileSystemUtility;
        private readonly IPhotoService _photoService;
        private readonly IPhotosUtility _photosUtility;
        private readonly IUnitOfWork _unitOfWork;

        public ChangeAvatarCommand(
            ILoggerUtility loggerUtility, 
            IFileSystemUtility fileSystemUtility,
            IPhotoService photoService,
            IPhotosUtility photosUtility,
            IUnitOfWork unitOfWork) : base(loggerUtility)
        {
            _fileSystemUtility = fileSystemUtility;
            _photoService = photoService;
            _photosUtility = photosUtility;
            _unitOfWork = unitOfWork;
        }

        protected override async Task<EmptyCommandResult> InternalExecute(ChangeAvatarCommandArgs arguments)
        {
            var result = new EmptyCommandResult() { IsSuccess = true };
            bool isEditing = false;
            string oldAbsolutePath = null;
            string newAbsolutePath = null;
            string newRelativePath = null;
            
            try
            {
                newAbsolutePath = _photosUtility.MoveToPhotosDirectory(arguments.TempAvatarPath);
                newRelativePath = _fileSystemUtility.GetRelativeFilePath(newAbsolutePath);
                var avatarEntity = await _photoService.GetAvatar(arguments.CurrentUser.CompanyId);

                isEditing = avatarEntity != null;

                if (isEditing)
                {
                    oldAbsolutePath = _fileSystemUtility.GetAbsoluteFilePath(avatarEntity.RelativePath);
                    avatarEntity.RelativePath = newRelativePath;                    
                }
                else
                {
                    _unitOfWork.GetRepository<Photo>().Add(new Photo()
                    {
                        CompanyId = arguments.CurrentUser.CompanyId,
                        PhotoType = PhotoType.Avatar,
                        RelativePath = newRelativePath
                    });
                }

                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _fileSystemUtility.DeleteFile(arguments.TempAvatarPath);
                _fileSystemUtility.DeleteFile(newAbsolutePath);
                throw;
            }

            if (isEditing)
            {
                DeleteOldAvatar(oldAbsolutePath);
            }

            return result;
        }

        protected override async Task<EmptyCommandResult> Validate(ChangeAvatarCommandArgs arguments)
        {
            return new EmptyCommandResult() {IsSuccess = true};
        }


        private void DeleteOldAvatar(string relativePath)
        {
            try
            {
                var absolutePath = _fileSystemUtility.GetAbsoluteFilePath(relativePath);              
                _fileSystemUtility.DeleteFile(absolutePath);
            }
            catch (Exception ex)
            {
                LoggerUtility.Warn($"Не удалось удалить старую аватарку {relativePath}", ex);
            }
        }
    }
}
