﻿namespace MakeupStudio.Business.Commands
{
    /// <summary>
    /// Аргументы для команды регистрации нового пользователя в админке
    /// </summary>
    public class RegistrationCommandArgs : BaseCommandArguments
    {
        public string Email { get; set; }

        /// <summary>
        /// Наименование мастерской
        /// </summary>
        public string Name { get; set; }

        public string Phone { get; set; }

        public string Password { get; set; }

        public string City { get; set; }

        /// <summary>
        /// Улица, где находится мастерская
        /// </summary>
        public string Street { get; set; }

        /// <summary>
        /// Номер дома
        /// </summary>
        public string House { get; set; }
    }
}
