﻿using System.Threading.Tasks;
using MakeupStudio.Common.Utils.Logger;
using MakeupStudio.Core.Entities;
using MakeupStudio.Core.Services;

namespace MakeupStudio.Business.Commands
{
    /// <summary>
    /// Команда изменения заказа
    /// </summary>
    internal class EditOrderCommand : BaseCommand<EditOrderCommandArgs, EmptyCommandResult>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IOrderService _orderService;
        private readonly IAccountService _accountService;
        private Order _order;

        public EditOrderCommand(
            ILoggerUtility loggerUtility,
            IUnitOfWork unitOfWork,
            IOrderService orderService, 
            IAccountService accountService) : base(loggerUtility)
        {
            _unitOfWork = unitOfWork;
            _orderService = orderService;
            _accountService = accountService;
        }

        protected override async Task<EmptyCommandResult> InternalExecute(EditOrderCommandArgs arguments)
        {
            var result = new EmptyCommandResult() { IsSuccess = true };

            _order.ClientName = arguments.ClientName;
            _order.ClientPhone = arguments.ClientPhone;
            _order.AppointedTime = arguments.AppointedTime;
            _order.MasterComment = arguments.MasterComment;
            await _unitOfWork.SaveChangesAsync();

            return result;
        }

        protected override async Task<EmptyCommandResult> Validate(EditOrderCommandArgs arguments)
        {
            var result = new EmptyCommandResult() { IsSuccess = true };

            //Проверяем что пользователь и фирма не заблокированы
            //
            var isActive = await _accountService.UserAndCompanyIsActive(
                arguments.CurrentUser.CompanyId, arguments.CurrentUser.Id);

            if (!isActive)
                return result.AddError(Errors.ErrorConstants.System.ACCESS_DENIED, 100);

            //Проверяем что заказ существует и он в валдином статусе
            _order = await _orderService.GetOrder(arguments.CurrentUser.CompanyId, arguments.OrderId);

            if (_order == null || _order.OrderState != OrderState.Accepted)
                return result.AddError(Errors.ErrorConstants.Orders.ORDER_NOT_FOUND, 100);

            //Проверяем могут ли поменять время приема
            //
            var canAppointMeeting = await _orderService.CanChangeAppointedTime(
                arguments.CurrentUser.CompanyId, arguments.OrderId, arguments.AppointedTime);

            if (!canAppointMeeting)
                return result.AddError(Errors.ErrorConstants.Orders.HAS_ANOTHER_ORDERS, 1000);

            return result;
        }
    }
}
