﻿using System;
using System.Threading.Tasks;
using MakeupStudio.Common.Utils.Logger;
using MakeupStudio.Core.Entities;
using MakeupStudio.Core.Services;

namespace MakeupStudio.Business.Commands
{
    /// <summary>
    /// Команда создания новой заявки - оставляется клиентом
    /// </summary>
    internal class CreateBidCommand : BaseCommand<CreateBidCommandArgs, EmptyCommandResult>
    {
        #region Поля конструктор 

        private readonly IUnitOfWork _unitOfWork;
        private readonly IOrderService _orderService;
        private readonly ICompanyService _companyService;

        public CreateBidCommand(
            ILoggerUtility loggerUtility,
           IUnitOfWork unitOfWork,
           IOrderService orderService,
           ICompanyService companyService) : base(loggerUtility)
        {
            _unitOfWork = unitOfWork;
            _orderService = orderService;
            _companyService = companyService;
        }

        #endregion


        protected override async Task<EmptyCommandResult> InternalExecute(CreateBidCommandArgs arguments)
        {
            var result = new EmptyCommandResult() { IsSuccess = true };

            var newBid = new Order()
            {
                CompanyId = arguments.CompanyId,
                ClientComment = arguments.ClientComment,
                ClientName = arguments.ClientName,
                ClientPhone = arguments.ClientPhone,
                CreatedDate = DateTime.UtcNow,
                OrderState = OrderState.NotAccepted
            };

            _unitOfWork.GetRepository<Order>().Add(newBid);
            await _unitOfWork.SaveChangesAsync();

            return result;
        }

        protected override async Task<EmptyCommandResult> Validate(CreateBidCommandArgs arguments)
        {
            var result = new EmptyCommandResult() {IsSuccess = true};

            var companyExists = await _companyService.CompanyIsExists(arguments.CompanyId);

            if (!companyExists)
                return result.AddError(Errors.ErrorConstants.Companies.NOT_FOUND, 100);

            //тут ошибки нет, проверка на существование идет в сутки
            //не больше 3х заявок от одной фирмы в сутки
            var bidExists = await _orderService.BidIsExists(arguments.CompanyId, arguments.ClientPhone);

            if (bidExists)
                return result.AddError(Errors.ErrorConstants.Orders.BID_ALREADY_EXISTS, 705);

            return result;
        }
    }
}
