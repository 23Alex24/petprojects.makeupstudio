﻿using System.Threading.Tasks;
using MakeupStudio.Common.Utils.Logger;
using MakeupStudio.Core.Entities;
using MakeupStudio.Core.Services;

namespace MakeupStudio.Business.Commands
{
    /// <summary>
    /// Команда удаления заказа
    /// </summary>
    internal class DeleteOrderCommand : BaseCommand<DeleteOrderCommandArgs, EmptyCommandResult>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IOrderService _orderService;
        private Order _order;

        public DeleteOrderCommand(
            ILoggerUtility loggerUtility,
            IUnitOfWork unitOfWork,
            IOrderService orderService) : base(loggerUtility)
        {
            _unitOfWork = unitOfWork;
            _orderService = orderService;
        }

        protected async override Task<EmptyCommandResult> InternalExecute(DeleteOrderCommandArgs arguments)
        {
            var result = new EmptyCommandResult() {IsSuccess = true};            
            var repo = _unitOfWork.GetRepository<Order>();
            repo.Remove(_order);
            await _unitOfWork.SaveChangesAsync();
            return result;
        }

        protected async override Task<EmptyCommandResult> Validate(DeleteOrderCommandArgs arguments)
        {
            var result = new EmptyCommandResult() {IsSuccess = true};
            _order = await _orderService.GetOrder(arguments.CurrentUser.CompanyId, arguments.OrderId);

            if (_order == null)
                return result.AddError(Errors.ErrorConstants.Orders.ORDER_NOT_FOUND, 100);

            return result;
        }
    }
}
