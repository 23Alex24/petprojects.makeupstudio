﻿using System;
using System.Threading.Tasks;
using MakeupStudio.Common.Utils.Logger;
using MakeupStudio.Core.Entities;
using MakeupStudio.Core.Services;

namespace MakeupStudio.Business.Commands
{
    /// <summary>
    /// Команда создания заказа
    /// </summary>
    internal class CreateOrderCommand : BaseCommand<CreateOrderCommandArgs, EmptyCommandResult>
    {
        private readonly IOrderService _orderService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAccountService _accountService;

        public CreateOrderCommand(
            ILoggerUtility loggerUtility,
            IOrderService orderService,
            IUnitOfWork unitOfWork,
            IAccountService accountService) : base(loggerUtility)
        {
            _orderService = orderService;
            _unitOfWork = unitOfWork;
            _accountService = accountService;
        }

        protected override async Task<EmptyCommandResult> InternalExecute(CreateOrderCommandArgs arguments)
        {
            var result = new EmptyCommandResult() { IsSuccess = true };

            var order = new Order()
            {
                CompanyId = arguments.CurrentUser.CompanyId,
                AppointedTime = arguments.AppointedTime,
                ClientName = arguments.ClientName,
                ClientPhone = arguments.ClientPhone,
                CreatedDate = DateTime.UtcNow,
                MasterComment = arguments.MasterComment,
                OrderState = OrderState.Accepted
            };

            _unitOfWork.GetRepository<Order>().Add(order);
            await _unitOfWork.SaveChangesAsync();

            return result;
        }

        protected override async Task<EmptyCommandResult> Validate(CreateOrderCommandArgs arguments)
        {
            var result = new EmptyCommandResult() { IsSuccess = true };

            //Проверяем что пользователь и фирма не заблокированы
            //
            var isActive = await _accountService.UserAndCompanyIsActive(
                arguments.CurrentUser.CompanyId, arguments.CurrentUser.Id);

            if (!isActive)
                return result.AddError(Errors.ErrorConstants.System.ACCESS_DENIED, 100);
            
            //Проверяем можно ли создать заказ на указанное время
            //
            var canCreate = await _orderService.CanChangeAppointedTime(
                arguments.CurrentUser.CompanyId, 0, arguments.AppointedTime);

            if (!canCreate)
                return result.AddError(Errors.ErrorConstants.Orders.HAS_ANOTHER_ORDERS, 1000);

            return result;
        }
    }
}
