﻿using System.Threading.Tasks;
using MakeupStudio.Common.Utils.Logger;
using MakeupStudio.Core.Services;

namespace MakeupStudio.Business.Commands.Work
{
    /// <summary>
    /// Команда редактирования услуги
    /// </summary>
    internal class EditWorkCommand : BaseCommand<EditWorkCommandArgs, EmptyCommandResult>
    {
        private readonly IWorkService _workService;
        private readonly IUnitOfWork _unitOfWork;
        private Core.Entities.Work _work;

        public EditWorkCommand(
            ILoggerUtility loggerUtility, 
            IWorkService workService, 
            IUnitOfWork unitOfWork) : base(loggerUtility)
        {
            _workService = workService;
            _unitOfWork = unitOfWork;
        }

        protected async override Task<EmptyCommandResult> InternalExecute(EditWorkCommandArgs arguments)
        {
            _work.Name = arguments.WorkName;
            _work.Price = arguments.Price;
            await _unitOfWork.SaveChangesAsync();
            return new EmptyCommandResult() {IsSuccess = true};
        }

        protected async override Task<EmptyCommandResult> Validate(EditWorkCommandArgs arguments)
        {
            var result = new EmptyCommandResult() {IsSuccess = true};
            arguments.WorkName = arguments.WorkName?.Trim();

            _work = await _workService.GetWork(arguments.WorkId, arguments.CurrentUser.CompanyId);

            if (_work == null)
                return result.AddError(Errors.ErrorConstants.Works.NOT_FOUND, 1000);

            if (!await _workService.CanChangeWorkName(arguments.WorkId, arguments.WorkName, _work.CategoryId))
                return result.AddError(Errors.ErrorConstants.Works.ALREADY_EXISTS, 100);

            return result;
        }
    }
}
