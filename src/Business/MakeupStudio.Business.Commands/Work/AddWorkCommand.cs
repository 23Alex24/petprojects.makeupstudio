﻿using System.Threading.Tasks;
using MakeupStudio.Common.Utils.Logger;
using MakeupStudio.Core.Services;

namespace MakeupStudio.Business.Commands.Work
{
    internal class AddWorkCommand : BaseCommand<AddWorkCommandArgs, EmptyCommandResult>
    {
        private readonly IWorkService _workService;
        private readonly ICompanyService _companyService;
        private readonly IUnitOfWork _unitOfWork;

        public AddWorkCommand(
            ILoggerUtility loggerUtility, 
            IWorkService workService, 
            ICompanyService companyService,
            IUnitOfWork unitOfWork) : base(loggerUtility)
        {
            _workService = workService;
            _companyService = companyService;
            _unitOfWork = unitOfWork;
        }

        protected async override Task<EmptyCommandResult> InternalExecute(AddWorkCommandArgs arguments)
        {
            var repository = _unitOfWork.GetRepository<Core.Entities.Work>();
            repository.Add(new Core.Entities.Work
            {
                CategoryId = arguments.CategoryId,
                Name = arguments.WorkName,
                CompanyId = arguments.CurrentUser.CompanyId,
                Price = arguments.Price
            });

            await _unitOfWork.SaveChangesAsync();
            return new EmptyCommandResult() {IsSuccess = true};
        }

        protected async override Task<EmptyCommandResult> Validate(AddWorkCommandArgs arguments)
        {
            var result = new EmptyCommandResult() {IsSuccess = true};
            arguments.WorkName = arguments.WorkName?.Trim();

            var workIsExists = await _workService.HasWorkName(arguments.CurrentUser.CompanyId, 
                arguments.CategoryId, arguments.WorkName);

            if (workIsExists)
                return result.AddError(Errors.ErrorConstants.Works.ALREADY_EXISTS, 1000);

            var category = await _workService.GetCategory(arguments.CategoryId);

            if (category == null || category.ParentCategoryId == null)
                return result.AddError(Errors.ErrorConstants.Works.CATEGORY_NOT_FOUND, 1000);

            return result;
        }
    }
}
