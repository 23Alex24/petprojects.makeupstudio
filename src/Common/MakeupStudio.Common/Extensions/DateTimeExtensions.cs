﻿using System;

namespace MakeupStudio.Common.Extensions
{
    public static class DateTimeExtensions
    {
        /// <summary>
        /// Приводит дату к UTC. Если DateTimeKind == Unspecified 
        /// считаем что она уже в UTC
        /// </summary>
        public static DateTime ToUtc(this DateTime date)
        {
            var result = date;
            if (result.Kind == DateTimeKind.Unspecified)
            {
                result = new DateTime(date.Year, date.Month, date.Day, date.Hour, date.Minute, date.Second, DateTimeKind.Utc);
                return result;
            }

            return result.ToUniversalTime();
        }

        /// <summary>
        /// Обнуляет секунды
        /// </summary>
        public static DateTime ResetSeconds(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, date.Hour, date.Minute, 0, date.Kind);
        }
    }
}
