﻿namespace MakeupStudio.Common.Utils.Photos
{
    /// <summary>
    /// Утилита для работы с картинками
    /// </summary>
    public interface IPhotosUtility
    {
        /// <summary>
        /// Генерирует случайное имя файла
        /// </summary>
        /// <param name="extension">Расширение файла вместе с точкой</param>
        string GenerateFileName(string extension);

        /// <summary>
        /// Перемещает файл в папку фотографий. Возвращает абсолютный путь к файлу (куда переместлии). 
        /// Имя файла может быть перегенерировано если файл с таким названием уже есть в папке фотографий
        /// </summary>
        /// <param name="absolutePath">Абсолютный путь к файлу, который нужно переместить в папку фотографий</param>
        string MoveToPhotosDirectory(string absolutePath);

        /// <summary>
        /// Возвращает абслютный урл к картинке исходя из относительного пути 
        /// </summary>
        /// <param name="relativePath">Относительный путь к картинке</param>
        string GetFileAbsoluteUrl(string relativePath);

        /// <summary>
        /// Изменяет размер картинки и возвращает абсолютный урл на измененную картинку
        /// </summary>
        /// <param name="absoluteUrl">Абсолютный урл к картинке</param>
        /// <param name="width">ширина в пикселях</param>
        string ResizeImage(string absoluteUrl, int width);

        /// <summary>
        /// Изменяет размер картинки на указанный и возвращает абсолютный урл на новую картинку.
        /// Картинку не обрезает а сжимает при необходимости
        /// </summary>
        /// <param name="absoluteUrl">Абсолютный урл к картинке</param>
        /// <param name="width">ширина в пикселях</param>
        /// <param name="height">высота в пикселях</param>
        string ResizeImage(string absoluteUrl, int width, int height);

        /// <summary>
        /// Возвращает относительный путь к картинке "No_image"
        /// </summary>
        string GetNoImageRelativePath();
    }
}
