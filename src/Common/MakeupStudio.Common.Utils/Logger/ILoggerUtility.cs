﻿using System;

namespace MakeupStudio.Common.Utils.Logger
{
    public interface ILoggerUtility
    {
        /// <summary>
        /// Логгирует сообщение трассировки
        /// </summary>
        /// <exception cref="LoggerException">
        /// Возникает если логгирование не удалось
        /// </exception>
        void Trace(string message, Exception ex = null);


        /// <summary>
        /// Логгирует сообщение отладки
        /// </summary>
        /// <exception cref="LoggerException">
        /// Возникает если логгирование не удалось
        /// </exception>
        void Debug(string message, Exception ex = null);


        /// <summary>
        /// Логгирует информационное сообщение
        /// </summary>
        /// <exception cref="LoggerException">
        /// Возникает если логгирование не удалось
        /// </exception>
        void Info(string message, Exception ex = null);


        /// <summary>
        /// Логгирует сообщение о предупреждении
        /// </summary>
        /// <exception cref="LoggerException">
        /// Возникает если логгирование не удалось
        /// </exception>
        void Warn(string message, Exception ex = null);


        /// <summary>
        /// Логгирует сообщение об ошибке
        /// </summary>
        /// <exception cref="LoggerException">
        /// Возникает если логгирование не удалось
        /// </exception>
        void Error(string message, Exception ex = null);


        /// <summary>
        /// Логгирует сообщение о критической ошибке
        /// </summary>
        /// <exception cref="LoggerException">
        /// Возникает если логгирование не удалось
        /// </exception>
        void Fatal(string message, Exception ex = null);
    }
}
