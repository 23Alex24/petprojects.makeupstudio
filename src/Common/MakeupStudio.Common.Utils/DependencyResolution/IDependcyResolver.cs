﻿namespace MakeupStudio.Common.Utils.DependencyResolution
{
    /// <summary>
    /// Резолвер зависимостей
    /// </summary>
    public interface IDependcyResolver
    {
        /// <summary>
        /// Создает или возвращает объект зависимости
        /// </summary>
        /// <typeparam name="T">Тип интерфейса, для которого необходимо предоставить объект</typeparam>
        /// <exception cref="DependencyResolverException">
        /// Может возникнуть при неправильной настройке IoC контейнера или при неудачной попытке резолвинга
        /// </exception>
        T Resolve<T>();
    }
}
