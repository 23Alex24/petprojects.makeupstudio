﻿using System;

namespace MakeupStudio.Common.Utils.Mapping
{
    /// <summary>
    /// Исключение маппинга объектов
    /// </summary>
    public sealed class MappingException : Exception
    {
        public MappingException()
        {
        }

        public MappingException(string message) : base(message)
        {
        }

        public MappingException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
