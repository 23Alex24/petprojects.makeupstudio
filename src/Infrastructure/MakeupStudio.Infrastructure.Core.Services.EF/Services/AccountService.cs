﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using MakeupStudio.Core.Entities;
using MakeupStudio.Core.Services;

namespace MakeupStudio.Infrastructure.Core.Services.EF
{
    internal class AccountService : BaseService, IAccountService
    {
        public AccountService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        /// <summary>
        /// Возвращает аккаунт по логину и паролю или null, если он не найден
        /// </summary>
        /// <param name="login">логин</param>
        /// <param name="passwordHash">хеш код пароля пользователя</param>
        public async Task<Account> GetAccountAsync(string login, string passwordHash)
        {
            if (string.IsNullOrWhiteSpace(login) || string.IsNullOrWhiteSpace(passwordHash))
                return null;

            var lowerLogin = login?.ToLower();
            var repository = UnitOfWork.GetRepository<Account>();

            return await repository.Query()
                .Where(x => x.Email.ToLower() == lowerLogin && x.PasswordHash == passwordHash)
                .FirstOrDefaultAsync();
        }

        /// <summary>
        /// Проверяет есть ли пользователь с таким логином
        /// </summary>
        public async Task<bool> UserIsExistsAsync(string login)
        {
            var repository = UnitOfWork.GetRepository<Account>();
            login = login?.ToLower();
            return await repository.Query()
                .Where(x => x.Email.ToLower() == login)
                .AnyAsync();
        }

        public async Task<bool> PhoneIsUsed(string phone)
        {
            var repository = UnitOfWork.GetRepository<Account>();
            phone = phone?.ToLower();
            return await repository.Query()
                .Where(x => x.Phone.ToLower() == phone)
                .AnyAsync();
        }

        public async Task<bool> UserAndCompanyIsActive(Guid companyId, int accountId)
        {
            return await UnitOfWork.GetRepository<Account>()
                .Query().AsNoTracking()
                .Where(x => x.CompanyId == companyId && x.Id == accountId && x.AccountState == AccountState.Active)
                .Where(x => x.Company.CompanyState == CompanyState.Active)
                .AnyAsync();
        }
    }
}
