﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using MakeupStudio.Core.Entities;
using MakeupStudio.Core.Services;
using MakeupStudio.Infrastructure.Core.Services.EF.Extensions.Queryable;

namespace MakeupStudio.Infrastructure.Core.Services.EF
{
    internal class WorkService : BaseService, IWorkService
    {
        public WorkService(IUnitOfWork unitOfWork) : base(unitOfWork) { }



        #region GetWorkCategories 

        public async Task<CategoryInfoDto[]> GetWorkCategoriesHierarchically()
        {
            var categories = await UnitOfWork.GetRepository<Category>()
                .Query().AsNoTracking()
                .Where(x => x.CategoryType == CategoryType.Works)                
                .OrderBy(x => x.Name)
                .Select(x => new CategoryInfoDto()
                {
                    Id = x.Id,
                    CategoryType = x.CategoryType,
                    ParentCategoryId = x.ParentCategoryId,
                    Name = x.Name
                })
                .ToListAsync();

            if (categories == null)
                return null;

            var parentCategories = categories.Where(x => x.ParentCategoryId == null).ToList();

            if (parentCategories == null)
                return null;

            var result = new List<CategoryInfoDto>();
            foreach (var parentCategory in parentCategories)
            {
                parentCategory.ChildCategories =
                    GetChildCategories(parentCategory, categories).OrderBy(x => x.Name).ToArray();
                result.Add(parentCategory);
            }

            return result.ToArray();
        }

        public async Task<Category[]> GetWorkCategories()
        {
            return await UnitOfWork.GetRepository<Category>()
                .Query().AsNoTracking()
                .Where(x => x.CategoryType == CategoryType.Works)
                .ToArrayAsync();
        }

        public async Task<Category> GetCategory(int categoryId)
        {
            return await UnitOfWork.GetRepository<Category>().Query().AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == categoryId);
        }

        private CategoryInfoDto[] GetChildCategories(CategoryInfoDto parentCategory, List<CategoryInfoDto> allCategories)
        {
            var childCategories = allCategories.Where(x => x.ParentCategoryId == parentCategory.Id).ToList();

            if (childCategories == null || childCategories.Count == 0)
                return null;

            var result = new List<CategoryInfoDto>();

            foreach (var childCategory in childCategories)
            {                
                childCategory.ChildCategories = GetChildCategories(childCategory, allCategories);
                result.Add(childCategory);
            }

            if (result.Count == 0)
                return null;

            return result.ToArray();
        }

        #endregion


        public async Task<PriceLimitsDto> GetPriceLimits()
        {
            var min = await UnitOfWork.GetRepository<Work>()
                .Query().AsNoTracking()
                .Where(x => x.Company.CompanyState == CompanyState.Active)
                .MinAsync(x => (decimal?) x.Price);

            var max = await UnitOfWork.GetRepository<Work>()
                .Query().AsNoTracking()
                .Where(x => x.Company.CompanyState == CompanyState.Active)
                .MaxAsync(x => (decimal?) x.Price);

            var result = new PriceLimitsDto();
            result.MinPrice = min.HasValue ? min.Value : Work.MIN_PRICE;
            result.MaxPrice = max.HasValue ? max.Value : Work.MAX_PRICE;            
            return result;
        }

        public async Task<SystemWorkName[]> GetSystemWorkNames()
        {
            return await UnitOfWork.GetRepository<SystemWorkName>().Query().AsNoTracking()
                .OrderBy(x=>x.Name)
                .ToArrayAsync();
        }

        public async Task<bool> HasWorkName(Guid companyId, int categoryId, string workName)
        {
            return await UnitOfWork.GetRepository<Work>().Query().AsNoTracking()
                .FilterByName(workName)
                .FilterByCompanyId(companyId)
                .FilterByCategoryId(categoryId)
                .AnyAsync();
        }

        public async Task<WorkPreviewDto[]> GetWorkPreviews(Guid companyId)
        {
            return await UnitOfWork.GetRepository<Work>().Query().AsNoTracking()
                .FilterByCompanyId(companyId)
                .Select(x => new WorkPreviewDto()
                {
                    Id = x.Id,
                    Price = x.Price,
                    WorkName = x.Name,
                    ChildCategoryName = x.Category.Name
                })
                .ToArrayAsync();
        }

        public async Task<Work> GetWork(int workId, Guid companyId)
        {
            return await UnitOfWork.GetRepository<Work>().Query()
                .FilterByCompanyId(companyId)
                .Where(x => x.Id == workId)
                .FirstOrDefaultAsync();
        }

        public async Task<bool> CanChangeWorkName(int workId, string newName, int categoryId)
        {
            var work = await UnitOfWork.GetRepository<Work>().Query().AsNoTracking()
                .FilterByCategoryId(categoryId)
                .FilterByName(newName)
                .Where(x => x.Id != workId)
                .FirstOrDefaultAsync();

            return work == null;
        }

        /// <summary>
        /// Возвращает информацию о минимальной цене на услуги для фирм
        /// </summary>
        /// <param name="companiesId">Айдишники фирм, для которых нужно предоставить информацию о мин. ценах</param>
        public async Task<CompanyMinPriceDto[]> GetCompaniesMinPrices(Guid[] companiesId)
        {
            var result = await UnitOfWork.GetRepository<Work>().Query().AsNoTracking()
                .Where(x => companiesId.Contains(x.CompanyId))
                .GroupBy(x => x.CompanyId)
                .Select(x => new CompanyMinPriceDto
                {
                    CompanyId = x.Key,
                    MinPrice = x.Min(y => y.Price)
                })
                .ToArrayAsync();

            return result;
        }

        public async Task<Work[]> GetCompanyWorks(Guid companyId)
        {
            return await UnitOfWork.GetRepository<Work>()
                .Query().AsNoTracking()
                .FilterByCompanyId(companyId)
                .ToArrayAsync();
        }
    }
}
