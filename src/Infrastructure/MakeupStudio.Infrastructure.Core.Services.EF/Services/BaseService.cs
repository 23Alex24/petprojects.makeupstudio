﻿using MakeupStudio.Core.Services;

namespace MakeupStudio.Infrastructure.Core.Services.EF
{
    /// <summary>
    /// Базовый сервис для работы с БД
    /// </summary>
    public abstract class BaseService
    {
        protected BaseService(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        protected IUnitOfWork UnitOfWork { get; }
    }
}
