﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using MakeupStudio.Core.Entities;
using MakeupStudio.Infrastructure.Core.Services.EF.SeedData;

namespace MakeupStudio.Infrastructure.Core.Services.EF
{
    internal class MakeupStudioContextConfiguration : DbMigrationsConfiguration<MakeupStudioContext>
    {
        public MakeupStudioContextConfiguration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(MakeupStudioContext context)
        {
            if (!context.Categories.Any())
            {
                HairdressingCategorySeedData.AddSeedData(context);
                NailCategorySeedData.AddSeedData(context);
                MakeupCategorySeedData.AddSeedData(context);
                CosmetologyCategorySeedData.AddSeedData(context);
                EpilationCategorySeedData.AddSeedData(context);
                MassageCategorySeedData.AddSeedData(context);
                EyelashesCategorySeedData.AddSeedData(context);
                context.SaveChanges();
            }

            if (!context.Companies.Any())
            {
                var category = context.Categories.Single(x => x.Name == "Массаж");
                var childCategories = context.Categories.Where(x => x.ParentCategoryId == category.Id).ToArray();

                var company = new Company()
                {
                    Id = Guid.NewGuid(),
                    Name = "Тестовая фирма",
                    Latitude = 55.741404D,
                    Longitude = 49.2165626D,
                    Balance = 1000,
                    City = "Казань",
                    CompanyState = CompanyState.Active,
                    CompanyType = CompanyType.Individual,
                    HouseNumber = "11",
                    Description = "Это тестовая фирма, созданная автоматически",
                    Street = "Дубравная",
                    Works = new List<Work>(),
                    Accounts = new List<Account>()
                    {
                        new Account()
                        {
                            Name = "Алёшка",
                            AccountState = AccountState.Active,
                            Email = "abc@mail.ru",
                            Phone = "+7-999-999-99-99",
                            PasswordHash = "2AF9B1BA42DC5EB01743E6B3759B6E4B" //Пароль Qwerty123
                        }
                    }
                };

                foreach (var childCategory in childCategories)
                {
                    company.Works.Add(new Work()
                    {
                        CategoryId = childCategory.Id,
                        Price = 100,
                        Name = "Тестовая услуга"
                    });
                }

                context.Companies.Add(company);
                context.SaveChanges();
            }


            base.Seed(context);
        }
    }
}
