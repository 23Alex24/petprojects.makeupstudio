﻿using System.Collections.Generic;
using MakeupStudio.Core.Entities;
using MakeupStudio.Infrastructure.Core.Services.EF.Extensions;

namespace MakeupStudio.Infrastructure.Core.Services.EF.SeedData
{
    /// <summary>
    /// Заполняет данные категории "Ногтевой сервис"
    /// </summary>
    internal class NailCategorySeedData
    {
        public static void AddSeedData(MakeupStudioContext context)
        {
            context.Categories.Add(new Category()
            {
                Name = "Ногтевой сервис",
                CategoryType = CategoryType.Works,
                ChildCategories = new Category[]
                {
                    CreateClassicManicureCategory(),
                    CreateHardwareManicureCategory(),
                    CreateCombinedManicureCategory(),
                    CreateClassicPedicureCategory(),
                    CreateHardwarePedicureCategory(),
                    CreateCombinedPedicureCategory()
                }
            });
        }


        /// <summary>
        /// Создает категорию "Классический маникюр"
        /// </summary>
        private static Category CreateClassicManicureCategory()
        {
            var category = new Category().SetName("Классический маникюр").SetCategoryType(CategoryType.Works);
            category.SystemWorkNames = new List<SystemWorkName>()
            {
                new SystemWorkName() {Name = "Классический (обрезной) маникюр"},
                new SystemWorkName() {Name = "Классический (обрезной) маникюр с покрытием шеллак"},
                new SystemWorkName() {Name = "Классический (обрезной) маникюр с дизайном"},
                new SystemWorkName() {Name = "Классический (обрезной) маникюр френч"},
            };
            return category;
        }

        /// <summary>
        /// Создает категорию "Аппаратный маникюр"
        /// </summary>
        private static Category CreateHardwareManicureCategory()
        {
            var category = new Category().SetName("Аппаратный маникюр").SetCategoryType(CategoryType.Works);
            category.SystemWorkNames = new List<SystemWorkName>()
            {
                new SystemWorkName() {Name = "Аппаратный маникюр с покрытием шеллак"},
                new SystemWorkName() {Name = "Аппаратный маникюр с дизайном"},
                new SystemWorkName() {Name = "Аппаратный маникюр френч"},
            };
            return category;
        }

        /// <summary>
        /// Создает категорию "Комбинированный маникюр"
        /// </summary>
        private static Category CreateCombinedManicureCategory()
        {
            var category = new Category().SetName("Комбинированный маникюр").SetCategoryType(CategoryType.Works);
            category.SystemWorkNames = new List<SystemWorkName>()
            {
                new SystemWorkName() {Name = "Комбинированный маникюр с покрытием шеллак"},
                new SystemWorkName() {Name = "Комбинированный маникюр с дизайном"},
                new SystemWorkName() {Name = "Комбинированный маникюр френч"},
            };
            return category;
        }

        /// <summary>
        /// Создает категорию "Классический педикюр"
        /// </summary>
        private static Category CreateClassicPedicureCategory()
        {
            var category = new Category().SetName("Классический педикюр").SetCategoryType(CategoryType.Works);
            category.SystemWorkNames = new List<SystemWorkName>()
            {
                new SystemWorkName() {Name = "Классический (обрезной) педикюр"},
                new SystemWorkName() {Name = "Классический (обрезной) педикюр с покрытием шеллак"},
                new SystemWorkName() {Name = "Классический (обрезной) педикюр с дизайном"},
                new SystemWorkName() {Name = "Классический (обрезной) педикюр френч"},
            };
            return category;
        }

        /// <summary>
        /// Создает категорию "Аппаратный педикюр"
        /// </summary>
        private static Category CreateHardwarePedicureCategory()
        {
            var category = new Category().SetName("Аппаратный педикюр").SetCategoryType(CategoryType.Works);
            category.SystemWorkNames = new List<SystemWorkName>()
            {
                new SystemWorkName() {Name = "Аппаратный педикюр с покрытием шеллак"},
                new SystemWorkName() {Name = "Аппаратный педикюр с дизайном"},
                new SystemWorkName() {Name = "Аппаратный педикюр френч"},
            };
            return category;
        }

        /// <summary>
        /// Создает категорию "Комбинированный педикюр"
        /// </summary>
        private static Category CreateCombinedPedicureCategory()
        {
            var category = new Category().SetName("Комбинированный педикюр").SetCategoryType(CategoryType.Works);
            category.SystemWorkNames = new List<SystemWorkName>()
            {
                new SystemWorkName() {Name = "Комбинированный педикюр с покрытием шеллак"},
                new SystemWorkName() {Name = "Комбинированный педикюр с дизайном"},
                new SystemWorkName() {Name = "Комбинированный педикюр френч"},
            };
            return category;
        }
    }
}