﻿using MakeupStudio.Core.Entities;
using MakeupStudio.Infrastructure.Core.Services.EF.Extensions;

namespace MakeupStudio.Infrastructure.Core.Services.EF.SeedData
{
    /// <summary>
    /// Данные для категории "Ресницы"
    /// </summary>
    internal static class EyelashesCategorySeedData
    {
        public static void AddSeedData(MakeupStudioContext context)
        {
            context.Categories.Add(new Category()
            {
                Name = "Ресницы",
                CategoryType = CategoryType.Works,
                ChildCategories = new Category[]
                {
                    new Category().SetName("Окрашивание ресниц").SetCategoryType(CategoryType.Works),
                    new Category().SetName("Ламинирование ресниц").SetCategoryType(CategoryType.Works),
                    new Category().SetName("Наращивание ресниц").SetCategoryType(CategoryType.Works),
                }
            });
        }
    }
}
