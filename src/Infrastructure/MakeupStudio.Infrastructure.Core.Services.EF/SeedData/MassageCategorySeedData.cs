﻿using MakeupStudio.Core.Entities;
using MakeupStudio.Infrastructure.Core.Services.EF.Extensions;

namespace MakeupStudio.Infrastructure.Core.Services.EF.SeedData
{
    /// <summary>
    /// Данные категории "Массаж"
    /// </summary>
    internal static class MassageCategorySeedData
    {
        public static void AddSeedData(MakeupStudioContext context)
        {
            context.Categories.Add(new Category()
            {
                Name = "Массаж",
                CategoryType = CategoryType.Works,
                ChildCategories = new Category[]
                {
                    new Category().SetName("Классический массаж").SetCategoryType(CategoryType.Works),
                    new Category().SetName("Лечебный массаж").SetCategoryType(CategoryType.Works),
                    new Category().SetName("Антицеллюлитный массаж").SetCategoryType(CategoryType.Works),
                    new Category().SetName("Медовый массаж").SetCategoryType(CategoryType.Works),
                    new Category().SetName("Вакуумный массаж").SetCategoryType(CategoryType.Works)
                }
            });
        }
    }
}
