﻿using MakeupStudio.Core.Entities;
using MakeupStudio.Infrastructure.Core.Services.EF.Extensions;

namespace MakeupStudio.Infrastructure.Core.Services.EF.SeedData
{
    /// <summary>
    /// Данные для категории "Брови"
    /// </summary>
    internal static class EyebrowsCategorySeedData
    {
        public static void AddSeedData(MakeupStudioContext context)
        {
            context.Categories.Add(new Category()
            {
                Name = "Брови",
                CategoryType = CategoryType.Works,
                ChildCategories = new Category[]
                {
                    new Category().SetName("Окрашивание бровей").SetCategoryType(CategoryType.Works),
                    new Category().SetName("Татуаж бровей").SetCategoryType(CategoryType.Works),
                    new Category().SetName("Микроблейдинг").SetCategoryType(CategoryType.Works),
                }
            });
        }
    }
}