﻿using System;
using MakeupStudio.Common.Utils.Logger;

namespace MakeupStudio.Infrastructure.Utils.Logger
{
    public class EmptyLoggerUtility : ILoggerUtility
    {
        public void Trace(string message, Exception ex = null)
        {
        }

        public void Debug(string message, Exception ex = null)
        {
        }

        public void Info(string message, Exception ex = null)
        {
        }

        public void Warn(string message, Exception ex = null)
        {
        }

        public void Error(string message, Exception ex = null)
        {
        }

        public void Fatal(string message, Exception ex = null)
        {
        }
    }
}
