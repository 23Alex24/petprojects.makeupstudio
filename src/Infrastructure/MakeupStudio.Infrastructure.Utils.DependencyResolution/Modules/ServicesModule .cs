﻿using System.Data.Entity;
using System.Linq;
using Autofac;
using MakeupStudio.Core.Services;
using MakeupStudio.Infrastructure.Core.Services.EF;
using MakeupStudio.Infrastructure.Core.Services.EF.UnitOfWork;
using Module = Autofac.Module;

namespace MakeupStudio.Infrastructure.Utils.DependencyResolution.Modules
{
    /// <summary>
    /// Модуль, в котором находятся DI настройки для сервисов, работающих с БД
    /// </summary>
    public class ServicesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MakeupStudioContext>().AsSelf().As<DbContext>().InstancePerRequest();
            builder.RegisterGeneric(typeof(EfRepository<>)).As(typeof(IRepository<>));
            builder.RegisterType<EfUnitOfWork>().As<IUnitOfWork>().InstancePerRequest();

            RegisterServices(builder);
        }

        private void RegisterServices(ContainerBuilder builder)
        {
            var baseServiceType = typeof(BaseService);

            var servicesTypes = baseServiceType.Assembly
                .GetTypes()
                .Where(x => x.IsSubclassOf(baseServiceType) && !x.IsAbstract)
                .ToList();

            foreach (var service in servicesTypes)
            {
                var serviceInterface = service.GetInterfaces().Single();
                builder.RegisterType(service).As(serviceInterface);
            }
        }
    }
}
