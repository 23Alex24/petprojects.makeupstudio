﻿using Autofac;
using MakeupStudio.Common.Utils.Cryptography;
using MakeupStudio.Common.Utils.FileSystem;
using MakeupStudio.Common.Utils.Geolocation;
using MakeupStudio.Common.Utils.Logger;
using MakeupStudio.Common.Utils.Photos;
using MakeupStudio.Infrastructure.Utils.Cryptography;
using MakeupStudio.Infrastructure.Utils.FileSystem;
using MakeupStudio.Infrastructure.Utils.Geolocation;
using MakeupStudio.Infrastructure.Utils.Logger;
using MakeupStudio.Infrastructure.Utils.Photos;

namespace MakeupStudio.Infrastructure.Utils.DependencyResolution.Modules
{
    public class UtilsModule : Module
    {
        private readonly string _baseDirectory;
        private readonly string _baseUri;

        /// <summary>
        /// Создает модуль
        /// </summary>
        /// <param name="baseDirectory">Базовая директория проекта</param>
        /// <param name="baseUri">Базовый урл сайта</param>
        public UtilsModule(string baseDirectory, string baseUri) : base()
        {
            _baseDirectory = baseDirectory;
            _baseUri = baseUri;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<GeolocationUtility>().As<IGeolocationUtility>();
            builder.RegisterType<CryptographyUtility>().As<ICryptographyUtility>();
            builder.RegisterType<EmptyLoggerUtility>().As<ILoggerUtility>();

            builder.RegisterType<FileSystemUtility>().As<IFileSystemUtility>()
                .WithParameter("baseDirectory", _baseDirectory);

            builder.RegisterType<PhotosUtility>().As<IPhotosUtility>()
                .WithParameter("baseUri", _baseUri);
        }
    }
}
