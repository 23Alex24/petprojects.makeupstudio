﻿using System.Linq;
using Autofac;
using MakeupStudio.Business.Commands;

namespace MakeupStudio.Infrastructure.Utils.DependencyResolution.Modules
{
    /// <summary>
    /// Модуль Autofac для настройки Business слоя
    /// </summary>
    public class BusinessModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            RegisterCommands(builder);
        }

        private void RegisterCommands(ContainerBuilder builder)
        {
            var interfaceType = typeof(ICommand<,>);
            var asm = interfaceType.Assembly;

            var classes = asm.GetTypes()
                .Where(x => x.IsClass)
                .Where(x => !x.IsAbstract);

            foreach (var asmClass in classes)
            {
                var commandInterface = asmClass.GetInterfaces()
                    .FirstOrDefault(x => x.IsGenericType && interfaceType == x.GetGenericTypeDefinition());

                if (commandInterface != null)
                    builder.RegisterType(asmClass).As(commandInterface);
            }            
        }
    }
}
