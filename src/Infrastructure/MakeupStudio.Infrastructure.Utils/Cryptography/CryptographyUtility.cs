﻿using System;
using System.Security.Cryptography;
using System.Text;
using MakeupStudio.Common.Utils.Cryptography;

namespace MakeupStudio.Infrastructure.Utils.Cryptography
{
    /// <summary>
    /// Утилита криптографии
    /// </summary>
    internal class CryptographyUtility : ICryptographyUtility
    {
        /// <summary>
        /// Вычисляет хэш код строки
        /// </summary>
        /// <param name="originalString">Исходная строка</param>
        /// <returns>Хеш код исходной строки</returns>
        public string GetHash(string originalString)
        {
            var md5 = new MD5CryptoServiceProvider();
            var originalBytes = Encoding.UTF8.GetBytes(originalString);
            var encodedBytes = md5.ComputeHash(originalBytes);
            return BitConverter.ToString(encodedBytes).Replace("-", String.Empty);
        }
    }
}
