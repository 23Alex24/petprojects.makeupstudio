﻿using System;
using System.IO;
using System.Linq;
using MakeupStudio.Common.Utils.FileSystem;

namespace MakeupStudio.Infrastructure.Utils.FileSystem
{
    /// <summary>
    /// Утилита для работы с файлами и папками
    /// </summary>
    internal class FileSystemUtility : IFileSystemUtility
    {
        #region Конструктор, поля, константы

        public const string FILES_DIRECTORY = "Files";
        public const string TEMP_DIRECTORY_NAME = "Temp";
        private readonly string _baseDirectory;

        /// <summary>
        /// Создает утилиту для работы с файлами и папками
        /// </summary>
        /// <param name="baseDirectory">Базовая папка проекта</param>
        public FileSystemUtility(string baseDirectory)
        {
            _baseDirectory = baseDirectory;

            if(!Directory.Exists(baseDirectory))
                throw new DirectoryNotFoundException("Базовая директория не существует");

            var filesDirPath = Path.Combine(_baseDirectory, FILES_DIRECTORY);
            CreateDirectory(filesDirPath);
            FilesDirectory = filesDirPath;
            var tempDirPath = Path.Combine(FilesDirectory, TEMP_DIRECTORY_NAME);
            CreateDirectory(tempDirPath);
            TempDirectory = tempDirPath;          
        }

        #endregion



        /// <summary>
        /// Возвращает абсолютный путь к Temp папке
        /// </summary>
        public string TempDirectory { get; private set; }

        /// <summary>
        /// Возвращает абсолютый путь к папке с файлами
        /// </summary>
        public string FilesDirectory { get; private set; }



        /// <summary>
        /// Проверяет создана ли директория
        /// </summary>
        /// <param name="absolutePath">Полный путь к папке</param>
        public bool DirectoryIsExists(string absolutePath)
        {
            if (string.IsNullOrWhiteSpace(absolutePath))
                return false;

            return Directory.Exists(absolutePath);
        }

        /// <summary>
        /// Создает папку и подпапки если она(они) еще не создана
        /// </summary>
        /// <param name="absolutePath">Абсолютный путь к создаваемой папке</param>
        public void CreateDirectory(string absolutePath)
        {
            if (Directory.Exists(absolutePath))
                return;

            Directory.CreateDirectory(absolutePath);
        }

        /// <summary>
        /// Проверяет создан ли файл
        /// </summary>
        /// <param name="absolutePath">Абсолютный путь к файлу</param>
        public bool FileIsExists(string absolutePath)
        {
            if (string.IsNullOrWhiteSpace(absolutePath))
                return false;

            return File.Exists(absolutePath);
        }

        /// <summary>
        /// Удаляет файл
        /// </summary>
        /// <param name="absolutePath">Абсолютный путь к файлу</param>
        public void DeleteFile(string absolutePath)
        {
            if (string.IsNullOrWhiteSpace(absolutePath))
                return;

            if (File.Exists(absolutePath))
                File.Delete(absolutePath);
        }

        /// <summary>
        /// Возвращает полный путь к файлу или null если такого файла нет. 
        /// </summary>
        /// <param name="relativePath">Относительный путь к файлу</param>
        public string GetAbsoluteFilePath(string relativePath)
        {
            if (string.IsNullOrWhiteSpace(relativePath))
                return null;

            var absolutePath = Path.Combine(_baseDirectory, relativePath);

            if (File.Exists(absolutePath))
                return absolutePath;

            return null;
        }

        /// <summary>
        /// Возвращает относительный путь к файлу
        /// </summary>
        /// <param name="absolutePath">Абсолютный путь к файлу</param>
        public string GetRelativeFilePath(string absolutePath)
        {
            if (string.IsNullOrWhiteSpace(absolutePath))
                return null;

            if (!File.Exists(absolutePath))
                return null;

            var entries = absolutePath.Split(new [] { _baseDirectory }, StringSplitOptions.RemoveEmptyEntries);
            if (entries.Any())
                return entries[0];

            return null;
        }


        /// <summary>
        /// Перемещает файл
        /// </summary>
        ///<param name="sourceAbsolutePath">Абсолютный путь к файлу</param>
        /// <param name="destinationAbsolutePath">Абсолютный путь файла куда нужно переместить файл.</param>
        public void MoveFile(string sourceAbsolutePath, string destinationAbsolutePath)
        {
            if (string.IsNullOrWhiteSpace(sourceAbsolutePath) || string.IsNullOrWhiteSpace(destinationAbsolutePath))
                return;

            File.Move(sourceAbsolutePath, destinationAbsolutePath);
        }

        /// <summary>
        /// Комбинирует путь и возвращает новый путь
        /// </summary>
        /// <param name="path1">Первая часть пути</param>
        /// <param name="path2">Вторая часть пути</param>
        public string CombinePath(string path1, string path2)
        {
            return Path.Combine(path1, path2);
        }

        /// <summary>
        /// Возвращает имя файла исходя из его абсолютного пути
        /// </summary>
        /// <param name="absolutePath">Абсолютный путь к файлу</param>
        public string GetFileName(string absolutePath)
        {
            return new FileInfo(absolutePath).Name;
        }

        /// <summary>
        /// Возвращает расширение файла с точкой
        /// </summary>
        /// <param name="absolutePath">абсолютный путь к файлу</param>
        public string GetFileExtension(string absolutePath)
        {
            return new FileInfo(absolutePath).Extension;   
        }
    }
}
