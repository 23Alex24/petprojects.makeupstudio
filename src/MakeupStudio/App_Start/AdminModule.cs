﻿using Autofac;
using MakeupStudio.ModelBuilders;

namespace MakeupStudio
{
    /// <summary>
    /// Модуль автофака для админки
    /// </summary>
    public class AdminModule: Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<PhotosIndexModelBuilder>().AsSelf();
            base.Load(builder);
        }
    }
}