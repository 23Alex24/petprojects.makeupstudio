﻿using System;
using System.Configuration;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using MakeupStudio.Common.Constants;
using MakeupStudio.Infrastructure.Utils.DependencyResolution.Modules;

namespace MakeupStudio
{
    public static class DependencyConfig
    {
        public static void Configure()
        {
            var builder = new ContainerBuilder();

            // Register your MVC controllers.
            builder.RegisterControllers(typeof(Global).Assembly);
            builder.RegisterModule<AdminModule>();
            builder.RegisterModule<AutofacWebTypesModule>();
            builder.RegisterModule<ServicesModule>();
            builder.RegisterModule<BusinessModule>();
            var utilsModule = new UtilsModule(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings[KeysConstants.BASE_FILES_URI]);
            builder.RegisterModule(utilsModule);

            // OPTIONAL: Enable property injection into action filters.
            builder.RegisterFilterProvider();

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}