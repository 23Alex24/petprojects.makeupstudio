﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using MakeupStudio.Attributes;
using MakeupStudio.Business.Commands;
using MakeupStudio.Business.Commands.Work;
using MakeupStudio.Core.Services;
using MakeupStudio.Extensions;
using MakeupStudio.ModelBuilders;
using MakeupStudio.Models.Works;
using MakeupStudio.Requests;
using MakeupStudio.Responses;

namespace MakeupStudio.Controllers
{
    /// <summary>
    /// Контроллер для работы с услугами
    /// </summary>
    [Authorize]    
    public class WorksController : BaseController
    {
        private readonly IWorkService _workService;

        public WorksController(IWorkService workService)
        {
            _workService = workService;
        }

        /// <summary>
        /// Страница услуг
        /// </summary>
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            var model = new WorksIndexModel()
            {
                Works = await _workService.GetWorkPreviews(CurrentUser.CompanyId)
            };

            if (model.Works == null || !model.Works.Any())
            {
                SetErrorMessage("У вас нет ни одной услуги. Добавьте услуги, чтобы клиенты могли оставлять заявки.");
                return View(model);
            }

            model.Works = model.Works.OrderBy(x => x.ChildCategoryName).ToArray();
            return View(model);
        }

        /// <summary>
        /// Страница создания новой услуги
        /// </summary>
        [HttpGet]
        public ActionResult CreateWork()
        {
            return View();
        }

        /// <summary>
        /// Метод добавления услуги
        /// </summary>
        [HttpPost, ValidateRequest]
        public async Task<JsonResult> CreateWork(CreateWorkRequest request)
        {
            var arguments = CreateCommandArgs<AddWorkCommandArgs>();
            arguments.Price = decimal.Parse(request.Price);
            arguments.WorkName = request.WorkName;
            arguments.CategoryId = request.CategoryId;

            var commandResult = await ExecuteCommand<AddWorkCommandArgs, EmptyCommandResult>(arguments);
            SetMessage(commandResult, "Услуга успешно добавлена");
            return Json(commandResult.ConverToResponse<EmptyResponse>());
        }

        /// <summary>
        /// Страница изменения услуг
        /// </summary>
        /// <param name="id">Id услуги</param>
        [HttpGet]
        public async Task<ActionResult> EditWork(int id)
        {
            var modelBuilder = new EditWorkModelBuilder(_workService);
            var model = await modelBuilder.Build(id, CurrentUser.CompanyId);

            if (model == null)
                return RedirectToAction("Index", "Works");

            return View(model);
        }

        /// <summary>
        /// Метод для редактирования услуги
        /// </summary>
        [HttpPost, ValidateRequest]
        public async Task<ActionResult> EditWork(EditWorkRequest request)
        {
            var arguments = CreateCommandArgs<EditWorkCommandArgs>();
            arguments.WorkId = request.WorkId;
            arguments.Price = decimal.Parse(request.Price);
            arguments.WorkName = request.WorkName;

            var commandResult = await ExecuteCommand<EditWorkCommandArgs, EmptyCommandResult>(arguments);
            SetMessage(commandResult, "Услуга успешно изменена");
            return Json(commandResult.ConverToResponse<EmptyResponse>());
        }

        /// <summary>
        /// Метод для получения списка категорий и системных названий услуг
        /// </summary>
        [HttpGet]
        public async Task<JsonResult> GetCategoriesAndSystemWorkNames()
        {
            var response= new GetCategoriesAndSystemWorkNamesModel();
            response.Categories = await _workService.GetWorkCategoriesHierarchically();
            response.SystemWorkNames = await _workService.GetSystemWorkNames();
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Удаляет услугу
        /// </summary>
        [HttpPost, ValidateRequest]
        public async Task<JsonResult> DeleteWork(DeleteWorkRequest request)
        {
            var args = CreateCommandArgs<DeleteWorkCommandArgs>();
            args.WorkId = request.WorkId;

            var commandResult = await ExecuteCommand<DeleteWorkCommandArgs, EmptyCommandResult>(args);
            SetMessage(commandResult, "Услуга успешно удалена");
            return Json(commandResult.ConverToResponse<EmptyResponse>());
        }
    }
}