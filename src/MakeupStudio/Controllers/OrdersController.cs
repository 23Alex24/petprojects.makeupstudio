﻿using System.Threading.Tasks;
using System.Web.Helpers;
using System.Web.Mvc;
using MakeupStudio.Attributes;
using MakeupStudio.Business.Commands;
using MakeupStudio.Common.Extensions;
using MakeupStudio.Core.Services;
using MakeupStudio.Extensions;
using MakeupStudio.ModelBuilders.Order;
using MakeupStudio.Models.Orders;
using MakeupStudio.Requests.Orders;
using MakeupStudio.Responses;

namespace MakeupStudio.Controllers
{
    /// <summary>
    /// Контроллер для работы с заявками и заказами
    /// </summary>
    [Authorize]
    public class OrdersController : BaseController
    {
        private readonly IOrderService _orderService;

        public OrdersController(IOrderService orderService)
        {
            _orderService = orderService;
        }



        #region Заявки

        /// <summary>
        /// Страница с необработанными заявками
        /// </summary>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Ajax метод для получения списка заявок
        /// </summary>
        [HttpGet]
        public async Task<JsonResult> GetNotAcceptedOrders()
        {
            var model = await _orderService.GetNotAcceptedOrders(CurrentUser.CompanyId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Ajax метод для удаления заказа
        /// </summary>
        [HttpPost]
        public async Task<JsonResult> DeleteOrder(int orderId, bool showMessage = false)
        {
            var commandArgs = CreateCommandArgs<DeleteOrderCommandArgs>();
            commandArgs.OrderId = orderId;
            var commandResult = await ExecuteCommand<DeleteOrderCommandArgs, EmptyCommandResult>(commandArgs);

            if (showMessage && commandResult.IsSuccess)
                SetSuccessMessage("Заказ успешно удален");

            return Json(commandResult.ConverToResponse<EmptyResponse>());
        }

        /// <summary>
        /// Метод для назначения приема клиента
        /// </summary>
        [HttpGet]
        public async Task<ActionResult> AppointMeeting(long orderId)
        {
            var modelBuilder = new AppointMeetingModelBuilder(_orderService);
            var model = await modelBuilder.Build(orderId, CurrentUser);

            if (model == null)
                return RedirectToAction("Index");

            return View(model);
        }

        /// <summary>
        /// Метод сохранения для страницы назначения приема клиента
        /// </summary>
        [HttpPost, ValidateRequest]
        public async Task<JsonResult> AppointMeeting(AppointMeetingRequest request)
        {
            var args = CreateCommandArgs<AppointMeetingCommandArgs>();
            args.AppointedTime = request.AppointedTime.ToUtc();
            args.MasterComment = request.MasterComment;
            args.OrderId = request.OrderId;

            var commandResult = await ExecuteCommand<AppointMeetingCommandArgs, EmptyCommandResult>(args);

            if (commandResult.IsSuccess)
                SetSuccessMessage(@"Заявка успешно принята. Она перемещена в раздел ""Заказы""");

            return Json(commandResult.ConverToResponse<EmptyResponse>());
        }

        #endregion



        #region Заказы

        /// <summary>
        /// Страница заказов
        /// </summary>
        [HttpGet]
        public async Task<ActionResult> UpcomingOrders()
        {
            var orders = await _orderService.GetAcceptedOrders(CurrentUser.CompanyId);
            return View(orders);
        }

        /// <summary>
        /// Метод для помечания заказа как "Выполнено"
        /// </summary>
        [HttpPost]
        public async Task<JsonResult> FinishOrder(long orderId)
        {
            var args = CreateCommandArgs<FinishOrderCommandArgs>();
            args.OrderId = orderId;
            var commandResult = await ExecuteCommand<FinishOrderCommandArgs, EmptyCommandResult>(args);

            if(commandResult.IsSuccess)
                SetSuccessMessage("Заказ успешно помечен как выполненный");

            return Json(commandResult.ConverToResponse<EmptyResponse>());
        }

        /// <summary>
        /// Страница редактирования заказа
        /// </summary>
        [HttpGet]
        public async Task<ActionResult> Edit(long orderId)
        {
            var builder = new EditOrderModelBuilder(_orderService);
            var model = await builder.Build(CurrentUser, orderId);

            if (model == null)
                return RedirectToAction("UpcomingOrders");

            return View(model);
        }

        /// <summary>
        /// Ajax метод на изменение заказа
        /// </summary>
        [HttpPost, ValidateRequest]
        public async Task<JsonResult> Edit(EditOrderModel request)
        {
            var args = CreateCommandArgs<EditOrderCommandArgs>();
            args.OrderId = request.OrderId;
            args.AppointedTime = request.AppointedTime.ToUtc();
            args.ClientName = request.ClientName;
            args.ClientPhone = request.ClientPhone;
            args.MasterComment = request.MasterComment;

            var commandResult = await ExecuteCommand<EditOrderCommandArgs, EmptyCommandResult>(args);

            if(commandResult.IsSuccess)
                SetSuccessMessage("Заказ успешно изменен");

            return Json(commandResult.ConverToResponse<EmptyResponse>());
        }

        /// <summary>
        /// Страница создания заказа
        /// </summary>
        [HttpGet]
        public async Task<ActionResult> Create()
        {            
            return View(new CreateOrderModel());
        }

        /// <summary>
        /// Ajax метод на создание заказа
        /// </summary>
        [HttpPost, ValidateRequest]
        public async Task<JsonResult> Create(CreateOrderModel request)
        {
            var args = CreateCommandArgs<CreateOrderCommandArgs>();
            args.AppointedTime = request.AppointedTime.ToUtc();
            args.ClientName = request.ClientName;
            args.ClientPhone = request.ClientPhone;
            args.MasterComment = request.MasterComment;

            var commandResult = await ExecuteCommand<CreateOrderCommandArgs, EmptyCommandResult>(args);

            if(commandResult.IsSuccess)
                SetSuccessMessage("Заказ успешно создан");

            return Json(commandResult.ConverToResponse<EmptyResponse>());
        }

        #endregion
    }
}