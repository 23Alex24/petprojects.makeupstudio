﻿namespace MakeupStudio.Requests
{
    public class DeleteWorkRequest
    {
        public int WorkId { get; set; }
    }
}