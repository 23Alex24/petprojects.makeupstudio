﻿using System.ComponentModel.DataAnnotations;
using MakeupStudio.Common.Constants;
using MakeupStudio.Core.Entities;

namespace MakeupStudio.Requests
{
    /// <summary>
    /// запрос для создания услуги
    /// </summary>
    public class CreateWorkRequest
    {
        /// <summary>
        /// Айди категории, куда надо добавлять услугу (эта категория не должна иметь подкатегорий)
        /// </summary>
        [Range(1, int.MaxValue, ErrorMessage = "Укажите категорию")]
        public int CategoryId { get; set; }

        /// <summary>
        /// Название услуги
        /// </summary>
        [Required(ErrorMessage = "Заполните название услуги", AllowEmptyStrings = false)]
        [MaxLength(Work.NAME_MAX_LENGTH, ErrorMessage = "Количество символов в названии услуги должно быть не больше {0}")]
        public string WorkName { get; set; }

        /// <summary>
        /// Стоимость услуги
        /// </summary>
        [RegularExpression(RegexConstants.NUMBER, ErrorMessage = "Стоимость услуги - целое число")]
        [Required(ErrorMessage = "Введите стоимость услуги")]
        [Range((double)Work.MIN_PRICE, (double)Work.MAX_PRICE, ErrorMessage = "Введите стоимость услуги в пределах от {1} до {2}")]
        public string Price { get; set; }
    }
}