﻿using System;
using System.ComponentModel.DataAnnotations;
using MakeupStudio.Attributes;
using MakeupStudio.Common.Constants;
using MakeupStudio.Core.Entities;

namespace MakeupStudio.Requests.Orders
{
    /// <summary>
    /// Запрос на запись клиента
    /// </summary>
    public class AppointMeetingRequest
    {
        /// <summary>
        /// Id заявки
        /// </summary>
        public long OrderId { get; set; }

        /// <summary>
        /// Время, на которое назначен прием для клиента (ко скольки клиент должен подойти) UTC
        /// </summary>
        [NotFromPastDate(ErrorMessage = "Нельзя назначить прием на прошедшее время")]

        public DateTime AppointedTime { get; set; }

        /// <summary>
        /// Комментарий мастера к заказу
        /// </summary>
        [MaxLength(Order.MASTER_COMMENT_MAX_LENGTH, ErrorMessage = "Максимальная длина комментария {0} символов")]
        [RegularExpression(RegexConstants.COMMENT, ErrorMessage = ValidationErrors.COMMENT_INCORRECT_FORMAT)]
        public string MasterComment { get; set; }
    }
}