﻿$(document).ready(function () {

    //=============================================================
    //=============================================================
    // Константы, переменные
    //=============================================================
    //=============================================================
    var $errorContainer = $("#error-msg-container");
    var $errorText = $("#error-msg");
    var localDateSelector = ".localdate";
    var finishBtnSelector = ".finishBtn";
    var deleteOrderBtnSelector = ".deleteOrder";

    //=============================================================
    //=============================================================
    // методы для работы с контролами
    //=============================================================
    //=============================================================

    //Обработчик нажатия на кнопке "Выполнено"
    //
    function finishOrder_OnClick() {
        if (confirm("Вы действительно хотите пометить заказ как выполненный?")) {
            var orderId = $(this).data("id");

            _finishOrder(orderId).done(function () {
                location.reload();
            });
        }        
    }

    //Обработчик нажатия на кнопке "Отменить"
    //
    function deleteOrder_OnClick() {
        if (confirm("Вы действительно хотите удалить заказ?")) {
            var orderId = $(this).data("id");

            _deleteOrder(orderId).done(function () {
                location.reload();
            });
        }
    }


    //=============================================================
    //=============================================================
    // AJAX методы и вспомогательные методы
    //=============================================================
    //=============================================================
    
    //ajax метод для смены статус заказа на "Выполнено"
    //
    function _finishOrder(orderId) {
        
        var $deferred = $.Deferred();

        $.ajax({
            type: "POST",
            url: "/Orders/FinishOrder",
            data : {
                orderId : orderId
            },
            success: function (data) {
                if (data.ErrorCode && data.ErrorCode != 0) {
                    _showError(data.ErrorMessage);
                    $deferred.reject();
                } else {
                    $deferred.resolve();
                }                
            },
            error: function () {
                $deferred.reject();
                _showError("Не удалось изменить статус заказа, попробуйте еще раз");
            }
        });

        return $deferred.promise();
    }

    //Ajax метод для удаления заявки
    //
    function _deleteOrder(orderId) {
        var $deferred = $.Deferred();

        $.ajax({
            type: "POST",
            url: "/Orders/DeleteOrder",
            data: {
                orderId: orderId,
                showMessage : true
            },
            success: function (data) {
                if (data.ErrorCode && data.ErrorCode != 0) {
                    _showError(data.ErrorMessage);
                } else {
                    $deferred.resolve();
                }
            },
            error: function () {
                $deferred.reject();
                _showError("Не удалось удалить заявку, попробуйте еще раз");
            }
        });

        return $deferred.promise();
    }

    //Устанавливаем локальное время в полях
    //
    function _setLocalTime() {
        var $dates = $(localDateSelector);

        $.each($dates, function (index, elem) {
            var $element = $(elem);
            var utcDateTime = $element.data("date");

            if (utcDateTime) {
                var localDate = new Date(utcDateTime);
                $element.text(_convertToLocalTime(localDate));
            }
        });
    }

    //Конвертирует дату в локальное время и формирует строку с датой
    //
    function _convertToLocalTime(localDate) {
        var date = new Date(localDate);
        var str = '';
        var val;

        str += (val = date.getDate()) < 10 ? "0" + val + "." : val + ".";
        str += (val = date.getMonth() + 1) < 10 ? "0" + val + "." : val + ".";
        str += date.getFullYear() + " ";

        str += (val = date.getHours()) < 10 ? "0" + val + ":" : val + ":";
        str += (val = date.getMinutes()) < 10 ? "0" + val : val;
        return str;
    }

    //Отображает ошибку
    //
    function _showError(errorMsg) {
        $errorText.text(errorMsg);
        $errorContainer.show();
    }

    //Скрывает ошибку
    //
    function _hideError() {
        $errorContainer.hide();
    }


    //=============================================================
    //=============================================================
    //INIT
    //=============================================================
    //=============================================================


    function init() {
        _setLocalTime();
        $(finishBtnSelector).click(finishOrder_OnClick);
        $(deleteOrderBtnSelector).click(deleteOrder_OnClick);
    }

    init();
});