﻿using System.Linq;
using System.Web.Mvc;

namespace MakeupStudio.Extensions
{
    /// <summary>
    /// Расширения на модел стейт
    /// </summary>
    public static class ModelStateExtensions
    {
        /// <summary>
        /// Получает первое сообщение об ошибке из ModelState
        /// </summary>
        public static string GetFirstError(this ModelStateDictionary modelState)
        {
            string result = null;
            if (!modelState.IsValid)
            {
                var value = modelState.Values.FirstOrDefault();
                result = value?.Errors
                    .Where(x => !string.IsNullOrWhiteSpace(x.ErrorMessage))
                    .Select(x=>x.ErrorMessage)
                    .FirstOrDefault();
            }

            return result;
        }
    }
}