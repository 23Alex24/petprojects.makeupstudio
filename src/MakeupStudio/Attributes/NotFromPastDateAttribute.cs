﻿using System;
using System.ComponentModel.DataAnnotations;
using MakeupStudio.Common.Extensions;

namespace MakeupStudio.Attributes
{
    /// <summary>
    /// Атрибут, проверяющий, что переданная дата не раньше, чем текущая дата и время
    /// </summary>
    public class NotFromPastDateAttribute : ValidationAttribute
    {
        public NotFromPastDateAttribute()
        {
            ErrorMessage = "Дата не может быть из прошлого";
        }

        public override bool IsValid(object value)
        {
            if (value == null)
                return false;

            var date = value as DateTime?;
            if (!date.HasValue)
                return true;

            var utcNow = DateTime.UtcNow.AddSeconds(-30);

            if (date.Value.ToUtc() < utcNow)
                return false;

            return true;
        }
    }
}