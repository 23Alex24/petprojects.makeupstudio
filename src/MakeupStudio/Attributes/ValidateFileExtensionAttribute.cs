﻿using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;

namespace MakeupStudio.Attributes
{
    /// <summary>
    /// Проверяет расширение файла
    /// </summary>
    public class ValidateFileExtensionAttribute : ValidationAttribute
    {
        private readonly string[] _extensions;

        /// <summary>
        /// Проверяет расширение файла. Расширения должны быть указаны с точкой, например, ".jpg"
        /// </summary>
        /// <param name="extensions">Допустимые расширения файла обязательно указывать с точкой.</param>
        public ValidateFileExtensionAttribute(params string[] extensions) : base()
        {
            _extensions = extensions
                .Where(x => !string.IsNullOrWhiteSpace(x))
                .Where(x => x[0] == '.')
                .Select(x => x.ToLowerInvariant())
                .ToArray();
        }

        public override bool IsValid(object value)
        {
            if (!_extensions.Any())
                return true;

            var file = value as HttpPostedFileBase;
            if (file != null)
            {
                string extension = Path.GetExtension(file.FileName).ToLower();
                return _extensions.Contains(extension);
            }

            return true;
        }
    }
}