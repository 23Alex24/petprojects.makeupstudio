﻿namespace MakeupStudio.Responses
{
    /// <summary>
    /// Базовый класс ответа контроллера (для ajax)
    /// </summary>
    public abstract class BaseResponse
    {
        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        public virtual string ErrorMessage { get; set; }

        /// <summary>
        /// Код ошибки
        /// </summary>
        public virtual int ErrorCode { get; set; }
    }
}