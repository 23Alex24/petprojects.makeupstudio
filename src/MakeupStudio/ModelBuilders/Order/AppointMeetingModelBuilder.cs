﻿using System.Threading.Tasks;
using MakeupStudio.Core.Entities;
using MakeupStudio.Core.Services;
using MakeupStudio.Models.Orders;

namespace MakeupStudio.ModelBuilders.Order
{
    /// <summary>
    /// Билдер для модели записи на прием клиента
    /// </summary>
    public class AppointMeetingModelBuilder
    {
        private readonly IOrderService _orderService;

        public AppointMeetingModelBuilder(IOrderService orderService)
        {
            _orderService = orderService;
        }

        /// <summary>
        /// Строит модель для отображения 
        /// </summary>
        /// <param name="orderId">Id заказа</param>
        public async Task<AppointMeetingModel> Build(long orderId, Account currentUser)
        {
            var order = await _orderService.GetNotAcceptedOrder(currentUser.CompanyId, orderId);

            if (order == null)
                return null;

            var result = new AppointMeetingModel()
            {
                OrderId = orderId,
                ClientName = order.ClientName,
                ClientPhone = order.ClientPhone,
                ClientComment = order.ClientComment,
                CreatedDate = order.CreatedDate,                                
            };

            return result;
        }
    }
}