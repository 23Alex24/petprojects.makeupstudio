﻿using System.Threading.Tasks;
using MakeupStudio.Common.Extensions;
using MakeupStudio.Core.Entities;
using MakeupStudio.Core.Services;
using MakeupStudio.Models.Orders;

namespace MakeupStudio.ModelBuilders.Order
{
    /// <summary>
    /// Строит модель для редактирования заказа
    /// </summary>
    public class EditOrderModelBuilder
    {
        private readonly IOrderService _orderService;

        public EditOrderModelBuilder(IOrderService orderService)
        {
            _orderService = orderService;
        }

        /// <summary>
        /// Строит модель для страницы редактирования заказа
        /// </summary>
        public async Task<EditOrderModel> Build(Account currentUser, long orderId)
        {
            var order = await _orderService.GetOrder(currentUser.CompanyId, orderId);

            if (order == null || order.OrderState != OrderState.Accepted)
                return null;

            var result = new EditOrderModel()
            {
                OrderId = orderId,
                AppointedTime = order.AppointedTime.Value.ToUtc(),
                CreatedDate = order.CreatedDate,
                ClientPhone = order.ClientPhone,
                ClientName = order.ClientName,
                ClientComment = order.ClientComment,
                MasterComment = order.MasterComment
            };

            return result;
        }
    }
}