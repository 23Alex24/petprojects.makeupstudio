﻿using System;
using System.Linq;
using System.Threading.Tasks;
using MakeupStudio.Core.Services;
using MakeupStudio.Models.Works;

namespace MakeupStudio.ModelBuilders
{
    /// <summary>
    /// Класс для построения модели EditWorkModel
    /// </summary>
    public class EditWorkModelBuilder
    {
        private readonly IWorkService _workService;

        public EditWorkModelBuilder(IWorkService workService)
        {
            _workService = workService;
        }

        public async Task<EditWorkModel> Build(int id, Guid companyId)
        {
            var work = await _workService.GetWork(id, companyId);

            if (work == null)
                return null;

            var categoriesInfo = await GetCategoriesInfo(work.CategoryId);

            if (categoriesInfo == null)
                return null;

            var model = new EditWorkModel()
            {
                WorkId = work.Id,
                Price = work.Price.ToString("F0"),
                WorkName = work.Name,
                ChildCategoryName = categoriesInfo.ChildCategoryName,
                CategoryName = categoriesInfo.MainCategoryName
            };

            var systemWorkNames = await _workService.GetSystemWorkNames();
            var categorySystemWorkNames = systemWorkNames.Where(x => x.CategoryId == work.CategoryId).ToArray();

            if (!categorySystemWorkNames.Any())
                return model;

            model.HasSystemWorkNames = true;
            model.SystemWorkNames = categorySystemWorkNames.Select(x => x.Name).ToArray();
            return model;
        }



        #region Вспомогательные методы 

        private async Task<CategoriesInfo> GetCategoriesInfo(int childCategoryId)
        {
            var categories = await _workService.GetWorkCategoriesHierarchically();

            foreach (var category in categories)
            {
                var childCategory = category.ChildCategories?.FirstOrDefault(x => x.Id == childCategoryId);

                if (childCategory != null)
                {
                    return new CategoriesInfo()
                    {
                        MainCategoryName = category.Name,
                        ChildCategoryName = childCategory.Name
                    };
                }
                    
            }

            return null;
        }

        /// <summary>
        /// Информация о категориях услуги
        /// </summary>
        private class CategoriesInfo
        {
            /// <summary>
            /// Основная главная категория
            /// </summary>
            public string MainCategoryName { get; set; }

            /// <summary>
            /// Непосредственная категория услуги
            /// </summary>
            public string ChildCategoryName { get; set; }
        }

        #endregion
    }
}