﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace MakeupStudio.HtmlHelpers
{
    public static class DisplayNameHtmlHelper
    {
        public static string GetDisplayName<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression)
        {
            var metaData = ModelMetadata.FromLambdaExpression<TModel, TProperty>(expression, htmlHelper.ViewData);
            string value = metaData.DisplayName ??
                           (metaData.PropertyName ?? ExpressionHelper.GetExpressionText(expression));

            return value;
        }
    }
}