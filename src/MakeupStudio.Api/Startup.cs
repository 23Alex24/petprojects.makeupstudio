﻿using System.Data.Entity;
using System.Net.Http.Formatting;
using System.Web.Http;
using MakeupStudio.Infrastructure.Core.Services.EF;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;

[assembly: OwinStartup(typeof(MakeupStudio.Api.Startup))]
namespace MakeupStudio.Api
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll);
            GlobalConfiguration.Configuration.Formatters.Clear();
            GlobalConfiguration.Configuration.Formatters.Add(new JsonMediaTypeFormatter());
            GlobalConfiguration.Configure(WebApiConfig.Register);            
            DependencyConfig.Configure(app);
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<MakeupStudioContext, MakeupStudioContextConfiguration>());
        }
    }
}
