﻿using System;

namespace MakeupStudio.Api.Model
{
    /// <summary>
    /// Информация о фирме
    /// </summary>
    public class CompanyInfoModel
    {
        public Guid Id { get; set; }

        /// <summary>
        /// Название фирмы
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание фирмы (это описание будут видеть пользователи)
        /// </summary>        
        public string Description { get; set; }

        /// <summary>
        /// Город, в котором находится фирма
        /// </summary>        
        public string City { get; set; }

        /// <summary>
        /// Улица, на которой находится фирма
        /// </summary>
        public string Street { get; set; }

        /// <summary>
        /// Номер дома фирмы
        /// </summary>
        public string HouseNumber { get; set; }

        /// <summary>
        /// Широта (местоположение фирмы)
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// Долгота (местоположение фирмы)
        /// </summary>
        public double Longitude { get; set; }
    }
}