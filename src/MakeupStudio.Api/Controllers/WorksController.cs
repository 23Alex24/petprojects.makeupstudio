﻿using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using MakeupStudio.Api.Responses.Works;
using MakeupStudio.Core.Services;

namespace MakeupStudio.Api.Controllers
{
    /// <summary>
    /// Контроллер для работы с услугами
    /// </summary>
    [RoutePrefix("Works")]   
    public sealed class WorksController : BaseApiController
    {
        private readonly IWorkService _workService;

        public WorksController(IWorkService workService)
        {
            _workService = workService;
        }

        [HttpGet, Route("Categories")]
        public async Task<JsonResult<CategoriesResponse>> GetCategories()
        {
            var response = new CategoriesResponse()
            {
                Categories = await _workService.GetWorkCategoriesHierarchically()
            };
            return Json(response);
        }

        [HttpGet, Route("PriceLimits")]
        public async Task<JsonResult<PriceLimitsResponse>> PriceLimits()
        {            
            var limits = await _workService.GetPriceLimits();
            var response = new PriceLimitsResponse()
            {
                MaxPrice = limits.MaxPrice,
                MinPrice = limits.MinPrice
            };
            return Json(response);
        }
    }
}