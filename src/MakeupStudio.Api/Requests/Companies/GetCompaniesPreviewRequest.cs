﻿namespace MakeupStudio.Api.Requests.Companies
{
    /// <summary>
    /// Запрос для получения превью по фирмам
    /// </summary>
    public class GetCompaniesPreviewRequest
    {
        /// <summary>
        /// Айдишники категорий услуг, по которым нужно отобразить фирмы
        /// </summary>
        public int[] CategoriesId { get; set; }

        /// <summary>
        /// Минимальная цена на услугу
        /// </summary>
        public decimal? MinPrice { get; set; }

        /// <summary>
        /// Максимальная цена на услугу
        /// </summary>
        public decimal? MaxPrice { get; set; }
    }
}