﻿using System;
using System.ComponentModel.DataAnnotations;
using MakeupStudio.Common.Constants;
using MakeupStudio.Core.Entities;

namespace MakeupStudio.Api.Requests.Orders
{
    /// <summary>
    /// Запрос на то, чтобы оставить заявку мастеру
    /// </summary>
    public class CreateBidRequest
    {
        /// <summary>
        /// Id фирмы
        /// </summary>
        public Guid CompanyId { get; set; }

        /// <summary>
        /// Телефон клиента
        /// </summary>
        [Required(ErrorMessage = "Укажите телефон", AllowEmptyStrings = false)]
        [RegularExpression(RegexConstants.PHONE, ErrorMessage = ValidationErrors.PHONE_INCORRECT_FORMAT)]
        public string ClientPhone { get; set; }

        /// <summary>
        /// Имя клиента
        /// </summary>
        [Required(ErrorMessage = "Заполните имя", AllowEmptyStrings = false)]
        [MaxLength(Company.NAME_MAX_LENGTH, ErrorMessage = "Слишком длинное имя")]
        [RegularExpression(RegexConstants.NAME, ErrorMessage = ValidationErrors.NAME_INCORRECT_FORMAT)]
        public string ClientName { get; set; }

        /// <summary>
        /// Комментарий клиента
        /// </summary>
        [MaxLength(Order.CLIENT_COMMENT_MAX_LENGTH, ErrorMessage = "Слишком длинный комментарий. Максимально {1} символов")]
        [RegularExpression(RegexConstants.COMMENT, ErrorMessage = ValidationErrors.COMMENT_INCORRECT_FORMAT)]
        public string ClientComment { get; set; }
    }
}