﻿using System.Threading.Tasks;
using MakeupStudio.Api.Model;
using MakeupStudio.Api.Responses.Companies;
using MakeupStudio.Common.Utils.Mapping;
using MakeupStudio.Core.Entities;
using MakeupStudio.Core.Services;

namespace MakeupStudio.Api.ResponseBuilders.Companies
{
    /// <summary>
    /// Билдер для респонса GetCompaniesForMapResonse (получение списка фирм для карты)
    /// </summary>
    public class GetCompaniesForMapResponseBuilder
    {
        private readonly IMapperUtility _mapperUtility;
        private readonly ICompanyService _companyService;

        public GetCompaniesForMapResponseBuilder(
            IMapperUtility mapperUtility,
            ICompanyService companyService)
        {
            _mapperUtility = mapperUtility;
            _companyService = companyService;
        }

        public async Task<GetCompaniesForMapResponse> Build()
        {
            var response = new GetCompaniesForMapResponse();
            var companies = await _companyService.GetActiveCompanies();

            if (companies.Length > 0)
                response.Companies = _mapperUtility.Map<Company[], CompanyInfoModel[]>(companies);

            return response;
        }
    }
}