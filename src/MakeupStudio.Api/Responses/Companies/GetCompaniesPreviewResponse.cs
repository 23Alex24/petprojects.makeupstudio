﻿using MakeupStudio.Api.Model;

namespace MakeupStudio.Api.Responses.Companies
{
    public class GetCompaniesPreviewResponse : BaseResponse
    {
        public CompanyPreviewModel[] Companies { get; set; }
    }
}