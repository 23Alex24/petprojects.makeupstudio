﻿using MakeupStudio.Core.Services;

namespace MakeupStudio.Api.Responses.Works
{
    public class CategoriesResponse : BaseResponse
    {
        public CategoryInfoDto[] Categories { get; set; }
    }
}