﻿using MakeupStudio.Api.Responses;
using MakeupStudio.Business.Commands;

namespace MakeupStudio.Api.Extensions
{
    /// <summary>
    /// Расширения для результатов команд
    /// </summary>
    public static class CommandResultExtensions
    {
        public static TResponse ConverToResponse<TResponse>(this ICommandResult commandResult)
            where TResponse : BaseResponse, new()
        {
            var result = new TResponse();
            result.ErrorCode = commandResult.ErrorCode;
            result.ErrorMessage = commandResult.IsSuccess ? null : commandResult.ErrorMessage;
            return result;
        }
    }
}